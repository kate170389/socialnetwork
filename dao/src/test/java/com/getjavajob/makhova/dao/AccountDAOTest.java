package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.common.*;
import com.getjavajob.makhova.dao.springdatajpa.AccountRepository;
import com.getjavajob.makhova.dao.springdatajpa.MessageRepository;
import com.getjavajob.makhova.utils.ValidationDAOException;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ApplicationConfig.class)
public class AccountDAOTest {
    private EntityManager em;
    @Autowired
    private RelationshipDAO rd;
    @Autowired
    private AccountRepository ad;
    @Autowired
    private MessageRepository md;

    @BeforeClass
    public static void onBeforeClass() {
        System.out.println("onBeforeClass");
    }

    @Before
    public void init() {
        try {
            em = Persistence.createEntityManagerFactory("default").createEntityManager();
            em.getTransaction().begin();
            Account accountOne = new Account("Ivan", "Ivanov", "Ivanovich", Gender.MALE,
                    "2011-08-07", "ivanov@mail.ru", "111");
            Phone phone = new Phone(accountOne, PhoneType.PERSONAL, "111");
            List<Phone> phones = new ArrayList<>();
            phones.add(phone);
            accountOne.setPhones(phones);
            Account accountTwo = new Account("Petr", "Petrov", "Petrovich", Gender.MALE,
                    "1989-03-17", "petrov@mail.ru", "111");
            em.persist(accountOne);
            em.persist(accountTwo);
            FriendRelation friendRelationOne = new FriendRelation(accountOne, accountTwo, FriendStatus.ACCEPTED, accountOne);
            em.persist(friendRelationOne);
            Message message1 = new Message(accountOne, accountTwo, "Hello!", null, MessageType.PERSONAL,
                    LocalDateTime.of(2019, 10, 10, 15, 15));
            em.persist(message1);
            Message message2 = new Message(accountTwo, accountOne, "Hello too!", null, MessageType.PERSONAL,
                    LocalDateTime.of(2019, 10, 11, 15, 15));
            em.persist(message2);
            Message message3 = new Message(accountOne, accountTwo, "How are you?", null, MessageType.COMMON,
                    LocalDateTime.of(2019, 10, 11, 16, 45));
            em.persist(message3);
            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
        System.out.println("initialisation before test");
    }

    @After
    public void destroy() {
        em.close();
        System.out.println("After test");
    }

    @AfterClass
    public static void onAfterClass() {
        System.out.println("After Class");
    }

    @Test
    public void getMessagesByTypeAndAccounts() {
        List<Message> expected = new ArrayList<>();
        expected.add(0, (md.findById(2).get()));
        expected.add(1, (md.findById(1).get()));
        assertEquals(expected, md.findByTypeAndAccounts(MessageType.PERSONAL, 1, 2, Sort.by(Sort.Direction.DESC, "creationDate")));
    }

    @Test
    public void getMessagesByTypeAndAccountTo() {
        List<Message> expected = new ArrayList<>();
        expected.add(0, (md.findById(3).get()));
        System.out.println(expected);
        assertEquals(expected, md.findByTypeAndAccountTo(MessageType.COMMON, ad.findById(2).get(), Sort.by(Sort.Direction.DESC, "creationDate")));
    }

    @Test
    public void delete() {
        int expectedSize = 1;
        rd.deleteRelations(2);
        md.deleteAllByAccountFromOrAccountTo(ad.findById(1).get(), ad.findById(1).get());
        ad.deleteById(2);
        long actualSize = ad.count();
        assertEquals(expectedSize, actualSize);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteEmptyResultData() {
        ad.deleteById(30);
    }

    @Test
    public void insert() {
        Account account = new Account();
        account.setName("Natalia");
        account.setSurName("Kirillova");
        account.setMiddleName("Kirillovna");
        account.setEmail("kirillova.ru");
        account.setGender(Gender.FEMALE);
        account.setBirthDate(LocalDate.of(1985, 2, 10));
        account.setPassword("1");
        Phone phone = new Phone(account, PhoneType.PERSONAL, "111");
        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        account.setPhones(phones);
        Account expected = account.clone();
        expected.setId(3);
        assertEquals(expected, ad.save(account));
        long expectedSize = 3;
        assertEquals(expectedSize, ad.count());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void insertDuplicateEmail() {
        Account account = new Account();
        account.setName("Natalia");
        account.setSurName("Kirillova");
        account.setMiddleName("Kirillovna");
        account.setEmail("ivanov@mail.ru");
        account.setGender(Gender.FEMALE);
        account.setBirthDate(LocalDate.of(1985, 2, 10));
        account.setPassword("1");
        Phone phone = new Phone(account, PhoneType.PERSONAL, "111");
        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        account.setPhones(phones);
        ad.save(account);
    }

    @Test
    public void get() {
        Account expected = new Account();
        expected.setName("Ivan");
        expected.setSurName("Ivanov");
        expected.setMiddleName("Ivanovich");
        expected.setGender(Gender.MALE);
        expected.setBirthDate(LocalDate.of(2011, 8, 7));
        expected.setEmail("ivanov@mail.ru");
        expected.setPassword("111");
        expected.setId(1);
        assertEquals(expected, ad.findById(1).get());
        assertEquals(expected, ad.findByEmail("ivanov@mail.ru"));
        assertEquals(false, ad.findById(20).isPresent());
    }

    @Test
    public void getElementsByString() {
        List<Account> expected = new ArrayList<>();
        assertEquals(expected, ad.findAllByString("ура", Sort.by("name")));
        expected.add(ad.findByEmail("ivanov@mail.ru"));
        assertEquals(expected, ad.findAllByString("iv", Sort.by("name")));
    }

    @Test
    public void update() {
        Account account = new Account("Ivan", "Ivanov", "Ivanovich", Gender.MALE,
                "2011-08-07", "ivanov@mail.ru", "111");
        account.setId(ad.findByEmail("ivanov@mail.ru").getId());
        Account expected = ad.findByEmail("ivanov@mail.ru").clone();
        expected.setPhones(null);
        assertEquals(expected, ad.save(account));
    }

    @Test
    public void insertRelation() throws ValidationDAOException {
        Account account = new Account();
        account.setName("Natalia");
        account.setSurName("Kirillova");
        account.setMiddleName("Kirillovna");
        account.setGender(Gender.FEMALE);
        account.setBirthDate(LocalDate.of(1985, 2, 10));
        account.setEmail("kirillova@mail.ru");
        account.setPassword("1");
        ad.save(account);
        FriendRelation relation = new FriendRelation(ad.findByEmail("ivanov@mail.ru"),
                ad.findByEmail("kirillova@mail.ru"),
                FriendStatus.ACCEPTED, ad.findByEmail("kirillova@mail.ru"));
        assertEquals(relation, rd.insert(relation));
        int expectedSize = 2;
        int actualSize = rd.getAll().size();
        assertEquals(expectedSize, actualSize);
    }

    @Test(expected = ValidationDAOException.class)
    public void insertRelationValidationDAOException() throws ValidationDAOException {
        FriendRelation relation = new FriendRelation(ad.findByEmail("ivanov@mail.ru"),
                ad.findByEmail("petrov@mail.ru"),
                FriendStatus.ACCEPTED, ad.findByEmail("petrov@mail.ru"));
        rd.insert(relation);
    }

    @Test
    public void deleteRelation() throws ValidationDAOException {
        int expectedSize = 0;
        int expected = 1;
        assertEquals(expected, rd.delete(rd.getByTwoId(ad.findByEmail("ivanov@mail.ru").getId(),
                ad.findByEmail("petrov@mail.ru").getId()).getId()));
        int actualSize = rd.getAll().size();
        assertEquals(expectedSize, actualSize);
    }

    @Test(expected = ValidationDAOException.class)
    public void deleteRelationValidationDAOException() throws ValidationDAOException {
        int expected = 0;
        assertEquals(expected, rd.delete(30));
    }

    @Test
    public void getRelation() {
        Account accountOne = ad.findByEmail("ivanov@mail.ru");
        Account accountTwo = ad.findByEmail("petrov@mail.ru");
        FriendRelation expected = new FriendRelation(accountOne, accountTwo, FriendStatus.ACCEPTED, accountOne);
        expected.setId(1);
        assertEquals(expected, rd.get(1));
        assertNull(rd.get(20));
    }

    @Test
    public void getAllRelations() {
        int expected = 1;
        assertEquals(expected, rd.getAll().size());
    }
}