package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.common.*;
import com.getjavajob.makhova.utils.ValidationDAOException;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ApplicationConfig.class)
public class GroupDaoTest {
    private EntityManager em;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private GroupRelationDAO groupRelation;

    @BeforeClass
    public static void onBeforeClass() {
        System.out.println("onBeforeClass");
    }

    @Before
    public void init() {
        try {
            em = Persistence.createEntityManagerFactory("default").createEntityManager();
            em.getTransaction().begin();
            Group groupOne = new Group("group1", "one", ZonedDateTime.now().toLocalDate());
            Group groupTwo = new Group("group2", "two", ZonedDateTime.now().toLocalDate());
            Group groupThree = new Group("group3", "three", ZonedDateTime.now().toLocalDate());
            Group groupFour = new Group("group4", "four", ZonedDateTime.now().toLocalDate());
            em.persist(groupOne);
            em.persist(groupTwo);
            em.persist(groupThree);
            em.persist(groupFour);
            Account account = new Account("Новый", "Ivanov", "Ivanovich", Gender.MALE,
                    "2011-08-07", "iv@mail.ru", "111");
            em.persist(account);
            GroupRelation relation1 = new GroupRelation(account, groupOne, GroupStatus.ADMIN);
            GroupRelation relation2 = new GroupRelation(account, groupTwo, GroupStatus.REQUEST);
            GroupRelation relation3 = new GroupRelation(account, groupThree, GroupStatus.USER);
            em.persist(relation1);
            em.persist(relation2);
            em.persist(relation3);
            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
        System.out.println("initialisation before test");
    }

    @After
    public void destroy() {
        em.close();
        System.out.println("After");
    }

    @AfterClass
    public static void onAfterClass() {
        System.out.println("After Class");
    }

    @Test
    @Transactional
    public void delete() throws ValidationDAOException {
        int expected = 1;
        int expectedSize = 3;
        assertEquals(expected, groupDao.delete(2));
        assertEquals(expectedSize, groupDao.getAll().size());
    }

    @Test(expected = ValidationDAOException.class)
    @Transactional
    public void deleteValidationDAOException() throws ValidationDAOException {
        int expected = 0;
        assertEquals(expected, groupDao.delete(5));
    }

    @Test
    @Transactional
    public void insert() throws ValidationDAOException {
        Group group = new Group("group4", "four", ZonedDateTime.now().toLocalDate());
        byte[] photo = {1, 2, 3};
        group.setPhoto(photo);
        Group expected = new Group("group4", "four", ZonedDateTime.now().toLocalDate());
        expected.setPhoto(photo);
        expected.setId(5);
        assertEquals(expected, groupDao.insert(group));
        int expectedSize = 5;
        assertEquals(expectedSize, groupDao.getAll().size());
    }

    @Test(expected = ValidationDAOException.class)
    @Transactional
    public void insertValidationDAOException() throws ValidationDAOException {
        Group group = new Group();
        group.setName("Four");
        group.setDescription("Inserted group");
        group.setDateOfCreation(ZonedDateTime.now().toLocalDate());
        group.setId(3);
        groupDao.insert(group);
    }

    @Test
    public void get() {
        Group expected = new Group("group1", "one", ZonedDateTime.now().toLocalDate());
        expected.setId(1);
        assertEquals(expected, groupDao.get(1));
        assertNull(groupDao.get(5));
    }

    @Test
    @Transactional
    public void update() throws ValidationDAOException {
        Group expected = groupDao.get(3);
        expected.setName("New");
        assertEquals(expected, groupDao.update(expected));
        assertEquals(expected, groupDao.get(3));
    }

    @Transactional
    @Test(expected = ValidationDAOException.class)
    public void updateValidationDAOException() throws ValidationDAOException {
        Group group = groupDao.get(1);
        group.setId(5);
        groupDao.update(group);
    }

    @Test
    public void getAll() {
        int expected = 4;
        assertEquals(expected, groupDao.getAll().size());
    }

    @Test
    public void getElementsByString() {
        List<Group> expected = new ArrayList<>();
        assertEquals(expected, groupDao.getElementsByString("mo"));
        expected.add(groupDao.get(1));
        assertEquals(expected, groupDao.getElementsByString("1"));
    }

    @Test
    @Transactional
    public void deleteRelation() throws ValidationDAOException {
        int expected = 1;
        int expectedSize = 2;
        assertEquals(expected, groupRelation.delete(2));
        assertEquals(expectedSize, groupRelation.getAll().size());
    }

    @Test(expected = ValidationDAOException.class)
    @Transactional
    public void deleteRelationValidationDAOException() throws ValidationDAOException {
        int expected = 0;
        assertEquals(expected, groupRelation.delete(5));
    }

    @Test
    @Transactional
    public void insertRelation() throws ValidationDAOException {
        Account account = groupRelation.get(1).getAccount();
        Group group = groupDao.get(4);
        GroupRelation expected = new GroupRelation(account, group, GroupStatus.USER);
        assertEquals(expected, groupRelation.insert(expected));
        int expectedSize = 4;
        assertEquals(expectedSize, groupRelation.getAll().size());
    }

    @Test
    @Transactional
    public void getByTwoId() {
        Account account = groupRelation.get(1).getAccount();
        Group group = groupDao.get(1);
        GroupRelation expected = new GroupRelation(account, group, GroupStatus.ADMIN);
        expected.setId(1);
        assertEquals(expected, groupRelation.getByTwoId(1, 1));
    }

    @Test(expected = ValidationDAOException.class)
    @Transactional
    public void insertRelationValidationDAOException() throws ValidationDAOException {
        GroupRelation relation = groupRelation.get(1);
        groupRelation.insert(relation);
    }

    @Test
    public void getRelation() {
        int expected = 1;
        assertEquals(expected, groupDao.get(1).getId());
        assertNull(groupDao.get(5));
    }

    @Test
    @Transactional
    public void updateRelation() throws ValidationDAOException {
        GroupRelation expected = groupRelation.get(3);
        expected.setStatus(GroupStatus.REQUEST);
        assertEquals(expected, groupRelation.update(expected));
        assertEquals(expected, groupRelation.get(3));
    }

    @Transactional
    @Test(expected = ValidationDAOException.class)
    public void updateRelationValidationDAOException() throws ValidationDAOException {
        GroupRelation relation = groupRelation.get(1);
        relation.setId(5);
        groupRelation.update(relation);
    }

    @Test
    public void getAllRelation() {
        int expected = 3;
        assertEquals(expected, groupRelation.getAll().size());
    }

    @Test
    @Transactional
    public void getRelationByTwoId() {
        GroupRelation expected = groupRelation.get(2);
        assertEquals(expected, groupRelation.getByTwoId(1, 2));
        assertNull(groupRelation.getByTwoId(1, 5));
    }

    @Test
    @Transactional
    public void getByGroupId() {
        List<GroupRelation> expected = new ArrayList<>();
        expected.add(groupRelation.get(2));
        assertEquals(expected, groupRelation.getByGroupId(2));
    }

    @Test
    @Transactional
    public void getByAccountId() {
        List<GroupRelation> expected = new ArrayList<>();
        expected.add(groupRelation.get(1));
        expected.add(groupRelation.get(2));
        expected.add(groupRelation.get(3));
        assertEquals(expected, groupRelation.getByAccountId(1));
    }
}