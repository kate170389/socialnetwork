package com.getjavajob.makhova.utils;

public class DuplicateDAOException extends RuntimeException {
    public DuplicateDAOException(Throwable couse) {
        super(couse);
    }
}
