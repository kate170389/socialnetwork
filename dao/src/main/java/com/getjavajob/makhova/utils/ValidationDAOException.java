package com.getjavajob.makhova.utils;

public class ValidationDAOException extends Exception {
    public ValidationDAOException(String msg) {
        super(msg);
    }

    public ValidationDAOException() {
    }

    public ValidationDAOException(Throwable couse) {
        super(couse);
    }
}