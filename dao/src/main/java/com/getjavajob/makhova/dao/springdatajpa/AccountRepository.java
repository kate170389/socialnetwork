package com.getjavajob.makhova.dao.springdatajpa;

import com.getjavajob.makhova.common.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {
    Account findByEmail(String email);

    @Query("select a from Account a where (upper(a.name) like concat('%',upper(:string),'%')  or upper(a.surName) like concat('%',upper(:string),'%'))")
    List<Account> findAllByString(@Param("string") String string, Sort sort);

    @Query("select a from Account a where (a.name like concat('%',:string,'%')  or a.surName like concat('%',:string,'%'))")
    List<Account> findAllByString(@Param("string") String string, Pageable page);
}