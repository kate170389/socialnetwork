package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.common.Group;
import com.getjavajob.makhova.utils.ValidationDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class GroupDao extends AbstractGenericDAO<Group> {
    private static final Logger logger = LoggerFactory.getLogger(GroupDao.class);

    @Override
    protected Class<Group> getEntityClass() {
        return Group.class;
    }

    @Override
    protected boolean isNew(Group group) {
        return group.getId() == 0;
    }

    @Override
    protected boolean isNotExist(Group group) throws ValidationDAOException {
        return get(group.getId()) == null;
    }

    public List<Group> getElementsByString(String string) {
        logger.debug("Start method {}.getElementsByString with string={}.", getEntityClass(), string);
        List<Group> groups = getCriteria(string).getResultList();
        logger.debug("End method {}.getElementsByString with string={}.", getEntityClass(), string);
        return groups;
    }

    public byte[] getPhoto(int id) {
        logger.debug("Start method {}.getPhoto with group id={}.", getEntityClass(), id);
        Group group = get(id);
        logger.debug("End method {}.getPhoto with group id={}.", getEntityClass(), id);
        return group != null ? group.getPhoto() : null;
    }

    public List<Group> getPaginationSearch(String string, int page, int pageSize) {
        logger.debug("Start method {}.getPaginationSearch with string for search={}, page={}, " +
                "number of entries per page={}.", getEntityClass(), string, page, pageSize);
        final int pageIndex = page - 1 < 0 ? 0 : page - 1;
        int fromRecordIndex = pageIndex * pageSize;
        TypedQuery<Group> criteria = getCriteria(string);
        criteria.setFirstResult(fromRecordIndex);
        criteria.setMaxResults(pageSize);
        List<Group> groups = criteria.getResultList();
        logger.debug("End method {}.getPaginationSearch with string for search={}, page={}, " +
                "number of entries per page={}.", getEntityClass(), string, page, pageSize);
        return groups;
    }

    private TypedQuery<Group> getCriteria(String string) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Group> cq = cb.createQuery(getEntityClass());
        Root<Group> from = cq.from(getEntityClass());
        CriteriaQuery<Group> select = cq.where(cb.like(from.get("name"), "%" + string + "%")).orderBy(cb.asc(from.get("name")));
        return em.createQuery(select);
    }
}