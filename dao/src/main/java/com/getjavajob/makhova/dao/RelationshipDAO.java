package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.common.FriendRelation;
import com.getjavajob.makhova.common.FriendStatus;
import com.getjavajob.makhova.utils.ValidationDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class RelationshipDAO extends AbstractGenericDAO<FriendRelation> {
    private static final Logger logger = LoggerFactory.getLogger(RelationshipDAO.class);

    @Override
    protected Class<FriendRelation> getEntityClass() {
        return FriendRelation.class;
    }

    @Override
    protected boolean isNew(FriendRelation friendRelation) throws ValidationDAOException {
        return getByTwoId(friendRelation.getAccountOne().getId(), friendRelation.getAccountTwo().getId()) == null;
    }

    @Override
    protected boolean isNotExist(FriendRelation friendRelation) throws ValidationDAOException {
        return get(friendRelation.getId()) == null;
    }

    public FriendRelation getByTwoId(int account_one_id, int account_two_id) {
        logger.debug("Start method {}.getByTwoId with first account id=={}, second account id=={}.", getEntityClass(),
                account_one_id, account_two_id);
        if (account_one_id > account_two_id) {
            int id = account_one_id;
            account_one_id = account_two_id;
            account_two_id = id;
        }
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<FriendRelation> cq = cb.createQuery(getEntityClass());
            Root<FriendRelation> from = cq.from(getEntityClass());
            CriteriaQuery<FriendRelation> select = cq.where(cb.and(cb.equal(from.get("accountOne"), account_one_id),
                    cb.equal(from.get("accountTwo"), account_two_id)));
            FriendRelation friendRelation = em.createQuery(select).getSingleResult();
            logger.debug("End method {}.getByTwoId with first account id=={}, second account id=={}.", getEntityClass(),
                    account_one_id, account_two_id);
            return friendRelation;
        } catch (NoResultException e) {
            logger.debug("End method {}.getByTwoId with first account id=={}, second account id=={}.", getEntityClass(),
                    account_one_id, account_two_id);
            return null;
        }
    }

    public List<FriendRelation> getFriends(int accountId) {
        logger.debug("Start method {}.getFriends for account with id=={}.", getEntityClass(), accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<FriendRelation> cq = cb.createQuery(getEntityClass());
        Root<FriendRelation> from = cq.from(getEntityClass());
        Predicate p = cb.or(cb.equal(from.get("accountOne"), accountId), cb.equal(from.get("accountTwo"), accountId));
        Predicate pr = cb.and(p, cb.equal(from.get("status"), FriendStatus.ACCEPTED));
        CriteriaQuery<FriendRelation> select = cq.where(pr);
        List<FriendRelation> friendRelations = em.createQuery(select).getResultList();
        logger.debug("End method {}.getFriends for account with id=={}.", getEntityClass(), accountId);
        return friendRelations;
    }

    public List<FriendRelation> getSentRequests(int accountId) {
        logger.debug("Start method {}.getSentRequests for account with id=={}.", getEntityClass(), accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<FriendRelation> cq = cb.createQuery(getEntityClass());
        Root<FriendRelation> from = cq.from(getEntityClass());
        Predicate p = cb.or(cb.equal(from.get("accountOne"), accountId), cb.equal(from.get("accountTwo"), accountId));
        Predicate pr = cb.and(p, cb.equal(from.get("status"), FriendStatus.PENDING),
                cb.equal(from.get("actionAccount"), accountId));
        CriteriaQuery<FriendRelation> select = cq.where(pr);
        List<FriendRelation> friendRelations = em.createQuery(select).getResultList();
        logger.debug("End method {}.getSentRequests for account with id=={}.", getEntityClass(), accountId);
        return friendRelations;
    }

    public List<FriendRelation> getDeclinedRequests(int accountId) {
        logger.debug("Start method {}.getDeclinedRequests for account with id=={}.", getEntityClass(), accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<FriendRelation> cq = cb.createQuery(getEntityClass());
        Root<FriendRelation> from = cq.from(getEntityClass());
        Predicate p = cb.or(cb.equal(from.get("accountOne"), accountId), cb.equal(from.get("accountTwo"), accountId));
        Predicate pr = cb.and(p, cb.equal(from.get("status"), FriendStatus.DECLINED),
                cb.notEqual(from.get("actionAccount"), accountId));
        CriteriaQuery<FriendRelation> select = cq.where(pr);
        List<FriendRelation> friendRelations = em.createQuery(select).getResultList();
        logger.debug("End method {}.getDeclinedRequests for account with id=={}.", getEntityClass(), accountId);
        return friendRelations;
    }

    public List<FriendRelation> getReceivedRequests(int accountId) {
        logger.debug("Start method {}.getReceivedRequests for account with id=={}.", getEntityClass(), accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<FriendRelation> cq = cb.createQuery(getEntityClass());
        Root<FriendRelation> from = cq.from(getEntityClass());
        Predicate p = cb.or(cb.equal(from.get("accountOne"), accountId), cb.equal(from.get("accountTwo"), accountId));
        Predicate pr = cb.and(p, cb.equal(from.get("status"), FriendStatus.PENDING),
                cb.notEqual(from.get("actionAccount"), accountId));
        CriteriaQuery<FriendRelation> select = cq.where(pr);
        List<FriendRelation> friendRelations = em.createQuery(select).getResultList();
        logger.debug("End method {}.getReceivedRequests for account with id=={}.", getEntityClass(), accountId);
        return friendRelations;
    }

    public List<FriendRelation> getBlocked(int accountId) {
        logger.debug("Start method {}.getBlocked for account with id=={}.", getEntityClass(), accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<FriendRelation> cq = cb.createQuery(getEntityClass());
        Root<FriendRelation> from = cq.from(getEntityClass());
        Predicate p = cb.or(cb.equal(from.get("accountOne"), accountId), cb.equal(from.get("accountTwo"), accountId));
        Predicate pr = cb.and(p, cb.equal(from.get("status"), FriendStatus.BLOCKED),
                cb.equal(from.get("actionAccount"), accountId));
        CriteriaQuery<FriendRelation> select = cq.where(pr);
        List<FriendRelation> friendRelations = em.createQuery(select).getResultList();
        logger.debug("End method {}.getBlocked for account with id=={}.", getEntityClass(), accountId);
        return friendRelations;
    }

    private List<FriendRelation> getRelations(int accountId) {
        logger.debug("Start method {}.getRelations for account with id=={}.", getEntityClass(), accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<FriendRelation> cq = cb.createQuery(getEntityClass());
        Root<FriendRelation> from = cq.from(getEntityClass());
        Predicate predicate = cb.or(cb.equal(from.get("accountOne"), accountId), cb.equal(from.get("accountTwo"),
                accountId));
        CriteriaQuery<FriendRelation> select = cq.where(predicate);
        List<FriendRelation> friendRelations = em.createQuery(select).getResultList();
        logger.debug("End method {}.getRelations for account with id=={}.", getEntityClass(), accountId);
        return friendRelations;
    }

    public int deleteRelations(int accountId) {
        logger.debug("Start method {}.deleteRelations for account with id=={}.", getEntityClass(), accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaDelete<FriendRelation> cd = cb.createCriteriaDelete(getEntityClass());
        Root<FriendRelation> from = cd.from(getEntityClass());
        Predicate predicate = cb.or(cb.equal(from.get("accountOne"), accountId), cb.equal(from.get("accountTwo"),
                accountId));
        CriteriaDelete<FriendRelation> delete = cd.where(predicate);
        int count = em.createQuery(delete).executeUpdate();
        logger.debug("End method {}.deleteRelations for account with id=={}.", getEntityClass(), accountId);
        return count;
    }
}