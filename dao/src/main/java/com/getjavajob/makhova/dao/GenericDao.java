package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.utils.ValidationDAOException;

import java.util.List;

public interface GenericDao<T> {

    T insert(T object) throws ValidationDAOException;

    T get(int id);

    T update(T object) throws ValidationDAOException;

    int delete(int id) throws ValidationDAOException;

    List<T> getAll();
}