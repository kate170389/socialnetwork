package com.getjavajob.makhova.dao.springdatajpa;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.common.Message;
import com.getjavajob.makhova.common.MessageType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {
    @Query("select a from Message a where (a.type = (:type) and (a.accountFrom.id=(:account1) and a.accountTo.id=(:account2) or a.accountFrom.id=(:account2) and a.accountTo.id=(:account1)))")
    List<Message> findByTypeAndAccounts(@Param("type") MessageType type, @Param("account1") Integer account1, @Param("account2") Integer account2, Sort sort);

    List<Message> findByTypeAndAccountTo(MessageType type, Account accountTo, Sort sort);

    void deleteAllByAccountFromOrAccountTo(Account accountFrom, Account accountTo);
}
