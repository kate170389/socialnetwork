package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.utils.ValidationDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AccountDAO extends AbstractGenericDAO<Account> {
    private static final Logger logger = LoggerFactory.getLogger(AccountDAO.class);

    @Override
    protected boolean isNew(Account account) {
        return account.getId() == 0;
    }

    public Account get(String email) {
        logger.debug("Start method {}.get with e-mail={}.", getEntityClass(), email);
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Account> cq = cb.createQuery(getEntityClass());
            Root<Account> from = cq.from(getEntityClass());
            CriteriaQuery<Account> select = cq.where(cb.equal(from.get("email"), email));
            Account account = em.createQuery(select).getSingleResult();
            logger.debug("End method {}.get with e-mail={}.", getEntityClass(), email);
            return account;
        } catch (NoResultException e) {
            logger.debug("End method {}.get with e-mail={}.", getEntityClass(), email);
            return null;
        }
    }

    public List<Account> getElementsByString(String string) {
        logger.debug("Start method {}.getElementsByString for string={}.", getEntityClass(), string);
        List<Account> accounts = getCriteria(string).getResultList();
        logger.debug("End method {}.getElementsByString for string={}", getEntityClass(), string);
        return accounts;
    }

    public int insertPhoto(byte[] logo, int id) {
        logger.debug("Start method {}.insertPhoto with account id={}.", getEntityClass(), id);
        Account account = em.find(Account.class, id);
        int count = 0;
        if (account != null) {
            account.setPhoto(logo);
            System.out.println(account);
            count++;
        }
        logger.debug("End method {}.insertPhoto with account id={}.", getEntityClass(), id);
        return count;
    }

    public byte[] getPhoto(int id) {
        logger.debug("Start method {}.getPhoto with account id={}.", getEntityClass(), id);
        Account account = get(id);
        logger.debug("End method {}.getPhoto with account id={}.", getEntityClass(), id);
        return account != null ? account.getPhoto() : null;
    }

    @Override
    protected Class<Account> getEntityClass() {
        return Account.class;
    }

    @Override
    protected boolean isNotExist(Account account) throws ValidationDAOException {
        return get(account.getId()) == null;
    }

    public List<Account> getPaginationSearch(String string, int page, int pageSize) {
        logger.debug("Start method {}.getPaginationSearch with string for search={}, page={}, " +
                "number of entries per page={}.", getEntityClass(), string, page, pageSize);
        final int pageIndex = page - 1 < 0 ? 0 : page - 1;
        int fromRecordIndex = pageIndex * pageSize;
        TypedQuery<Account> criteria = getCriteria(string);
        criteria.setFirstResult(fromRecordIndex);
        criteria.setMaxResults(pageSize);
        List<Account> accounts = criteria.getResultList();
        logger.debug("End method {}.getPaginationSearch with string for search={}, page={}, " +
                "number of entries per page={}.", getEntityClass(), string, page, pageSize);
        return accounts;
    }

    private TypedQuery<Account> getCriteria(String string) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(getEntityClass());
        Root<Account> from = cq.from(getEntityClass());
        CriteriaQuery<Account> select = cq.where(cb.or(cb.like(from.get("name"), "%" + string + "%"),
                cb.like(from.get("surName"), "%" + string + "%"))).orderBy(cb.asc(from.get("name")));
        return em.createQuery(select);
    }
}