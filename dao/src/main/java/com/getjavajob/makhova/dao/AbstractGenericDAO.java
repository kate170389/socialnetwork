package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.utils.DuplicateDAOException;
import com.getjavajob.makhova.utils.ValidationDAOException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public abstract class AbstractGenericDAO<T> implements GenericDao<T> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractGenericDAO.class);
    @PersistenceContext
    EntityManager em;

    protected abstract Class<T> getEntityClass();

    protected abstract boolean isNew(T object) throws ValidationDAOException;

    protected abstract boolean isNotExist(T object) throws ValidationDAOException;

    public int delete(int id) throws ValidationDAOException {
        logger.debug("Start method delete in {} with id={}.", getEntityClass(), id);
        int count = 0;
        T object = em.find(getEntityClass(), id);
        if (object != null) {
            em.remove(object);
            count++;
        } else {
            throw new ValidationDAOException("Object for delete with id=" + id + " not found.");
        }
        logger.debug("End  method delete in {} with id={}.", getEntityClass(), id);
        return count;
    }

    public T insert(T object) throws ValidationDAOException {
        logger.debug("Start method insert in {}.", getEntityClass());
        try {
            if (isNew(object)) {
                em.persist(object);
                logger.debug("End of method insert in{}.", getEntityClass());
                return object;
            } else {
                throw new ValidationDAOException("Object already exists.");
            }
        } catch (PersistenceException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                throw new DuplicateDAOException(e);
            } else throw e;
        }
    }

    public T get(int id) {
        logger.debug("Start method get in {}. with id={}", getEntityClass(), id);
        T object = em.find(getEntityClass(), id);
        logger.debug("End of method get in {} with id={}.", getEntityClass(), id);
        return object;
    }

    public List<T> getAll() {
        logger.debug("Start method getAll in {}.", getEntityClass());
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(getEntityClass());
        Root<T> from = cq.from(getEntityClass());
        CriteriaQuery<T> select = cq.select(from);
        List<T> result = em.createQuery(select).getResultList();
        logger.debug("End of method getAll in {}.", getEntityClass());
        return result;
    }

    public T update(T object) throws ValidationDAOException {
        logger.debug("Start method update in {}.", getEntityClass());
        try {
            if (isNotExist(object)) {
                throw new ValidationDAOException("Object for update not found");
            }
            T updatedObject = em.merge(object);
            em.flush();
            logger.debug("End method update in {}.", getEntityClass());
            return updatedObject;
        } catch (PersistenceException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                throw new DuplicateDAOException(e);
            } else {
                throw e;
            }
        }
    }
}