package com.getjavajob.makhova.dao;

import com.getjavajob.makhova.common.GroupRelation;
import com.getjavajob.makhova.utils.ValidationDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class GroupRelationDAO extends AbstractGenericDAO<GroupRelation> {
    private static final Logger logger = LoggerFactory.getLogger(GroupRelationDAO.class);

    @Override
    protected Class<GroupRelation> getEntityClass() {
        return GroupRelation.class;
    }

    @Override
    protected boolean isNew(GroupRelation groupRelation) throws ValidationDAOException {
        return getByTwoId(groupRelation.getAccount().getId(), groupRelation.getGroup().getId()) == null;
    }

    @Override
    protected boolean isNotExist(GroupRelation groupRelation) throws ValidationDAOException {
        return get(groupRelation.getId()) == null;
    }

    public GroupRelation getByTwoId(int account_id, int group_id) {
        logger.debug("Start method {}.getByTwoId with account id=={}, group id={}.", getEntityClass(),
                account_id, group_id);
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<GroupRelation> cq = cb.createQuery(getEntityClass());
            Root<GroupRelation> from = cq.from(getEntityClass());
            CriteriaQuery<GroupRelation> select = cq.where(cb.and(cb.equal(from.get("account"), account_id),
                    cb.equal(from.get("group"), group_id)));
            GroupRelation groupRelation = em.createQuery(select).getSingleResult();
            logger.debug("End method {}.getByTwoId with account id=={}, group id={}.", getEntityClass(),
                    account_id, group_id);
            return groupRelation;
        } catch (NoResultException e) {
            logger.debug("End method {}.getByTwoId with account id=={}, group id={}.", getEntityClass(),
                    account_id, group_id);
            return null;
        }
    }

    public List<GroupRelation> getByGroupId(int id) {
        logger.debug("Start method {}.getByGroupId with group id={}.", getEntityClass(), id);
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<GroupRelation> cq = cb.createQuery(getEntityClass());
            Root<GroupRelation> from = cq.from(getEntityClass());
            CriteriaQuery<GroupRelation> select = cq.where(cb.equal(from.get("group"), id));
            List<GroupRelation> groupRelations = em.createQuery(select).getResultList();
            logger.debug("End method {}.getByGroupId with group id={}.", getEntityClass(), id);
            return groupRelations;
        } catch (NoResultException e) {
            logger.debug("End method {}.getByGroupId with group id={}.", getEntityClass(), id);
            return null;
        }
    }

    public List<GroupRelation> getByAccountId(int id) {
        logger.debug("Start method {}.getByAccountId with account id={}.", getEntityClass(), id);
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<GroupRelation> cq = cb.createQuery(getEntityClass());
            Root<GroupRelation> from = cq.from(getEntityClass());
            CriteriaQuery<GroupRelation> select = cq.where(cb.equal(from.get("account"), id));
            List<GroupRelation> groupRelations = em.createQuery(select).getResultList();
            logger.debug("End method {}.getByAccountId with account id={}.", getEntityClass(), id);
            return groupRelations;
        } catch (NoResultException e) {
            logger.debug("End method {}.getByAccountId with account id={}.", getEntityClass(), id);
            return null;
        }
    }
}