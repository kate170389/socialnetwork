package com.getjavajob.makhova.common;

import java.io.Serializable;

public class JMSFriendRequest implements Serializable {
    public static final String COLLECTION_NAME = "accounts";

    private String id;
    private String emailFrom;
    private String emailTo;
    private String date;

    public JMSFriendRequest() {
    }

    public JMSFriendRequest(String emailFrom, String emailTo, String date) {
        this.emailFrom = emailFrom;
        this.emailTo = emailTo;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    @Override
    public String toString() {
        return "JMSFriendRequest{" +
                "id='" + id + '\'' +
                ", emailFrom='" + emailFrom + '\'' +
                ", emailTo='" + emailTo + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}