package com.getjavajob.makhova.common;

import javax.xml.bind.annotation.XmlEnumValue;

public enum Gender {
    @XmlEnumValue(value = "MALE")
    MALE(),
    @XmlEnumValue(value = "FEMALE")
    FEMALE();

    public static Gender findName(String name) {
        for (Gender gender : values()) {
            if (gender.name().equals(name)) {
                return gender;
            }
        }
        return null;
    }
}