package com.getjavajob.makhova.common;

public enum Role {
    USER,
    ADMIN;
}