package com.getjavajob.makhova.common;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "account_group")
public class GroupRelation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", nullable = false)
    private Group group;
    @Enumerated(EnumType.STRING)
    private GroupStatus status;

    public GroupRelation() {
    }

    public GroupRelation(Account account, Group group, GroupStatus status) {
        this.account = account;
        this.group = group;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GroupStatus getStatus() {
        return status;
    }

    public void setStatus(GroupStatus status) {
        this.status = status;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupRelation that = (GroupRelation) o;
        return id == that.id &&
                Objects.equals(account.getId(), that.account.getId()) &&
                Objects.equals(group, that.group) &&
                status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, group, status);
    }

    @Override
    public String toString() {
        return "GroupRelation{" +
                "id=" + id +
                ", account=" + account +
                ", group=" + group +
                ", status=" + status +
                '}';
    }
}