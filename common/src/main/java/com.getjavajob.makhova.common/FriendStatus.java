package com.getjavajob.makhova.common;

public enum FriendStatus {
    PENDING(),
    ACCEPTED(),
    DECLINED(),
    BLOCKED();

    public static FriendStatus findName(String name) {
        for (FriendStatus status : values()) {
            if (status.name().equals(name)) {
                return status;
            }
        }
        return null;
    }
}