package com.getjavajob.makhova.common;

import javax.xml.bind.annotation.XmlEnumValue;

public enum PhoneType {
    @XmlEnumValue(value = "WORK")
    WORK(),
    @XmlEnumValue(value = "PERSONAL")
    PERSONAL();

    public boolean isWork() {
        return this == WORK;
    }

    public boolean isPersonal() {
        return this == PERSONAL;
    }
}