package com.getjavajob.makhova.common;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_from")
    private Account accountFrom;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_to")
    private Account accountTo;
    private String text;
    private byte[] photo;
    @Enumerated(STRING)
    private MessageType type;
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    public Message(Account accountFrom, Account accountTo, String text, byte[] photo, MessageType type, LocalDateTime creationDate) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.text = text;
        this.photo = photo;
        this.type = type;
        this.creationDate = creationDate;
    }

    public Message() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", accountFrom=" + accountFrom.getId() +
                ", accountTo=" + accountTo.getId() +
                ", text='" + text + '\'' +
                ", type=" + type +
                ", creation date=" + creationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return id == message.id &&
                Objects.equals(accountFrom, message.accountFrom) &&
                Objects.equals(accountTo, message.accountTo) &&
                Objects.equals(text, message.text) &&
                type == message.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountFrom, accountTo, text, type);
    }
}