package com.getjavajob.makhova.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlType(propOrder = {"id", "name", "surName", "middleName", "gender", "phones", "birthDate", "homeAddress", "workAddress",
        "email", "icq", "skype", "additionalInfo"})
@Entity
@Table(name = "accounts")
public class Account implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surName;
    private String middleName;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @JsonManagedReference
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Phone> phones = new ArrayList<>();
    @Column(name = "birthDay")
    private LocalDate birthDate;
    private String homeAddress;
    private String workAddress;
    @Column(unique = true, nullable = false)
    private String email;
    private String icq;
    private String skype;
    private String additionalInfo;
    @Column(nullable = false)
    private String password;
    @Column(name = "logo")
    private byte[] photo;
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private Role role;
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonIgnore
    private List<GroupRelation> groups = new ArrayList<>();

    public Account(String name, String surName, String middleName, Gender gender, String birthDate, String email,
                   String password) {
        this.name = name;
        this.surName = surName;
        this.middleName = middleName;
        this.gender = gender;
        this.birthDate = LocalDate.parse(birthDate);
        this.email = email;
        this.password = password;
        this.role = Role.USER;
    }

    public Account() {
    }

    //region Getters
    @XmlTransient
    public byte[] getPhoto() {
        return photo;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getSurName() {
        return surName;
    }

    @XmlElementWrapper(name = "phones")
    @XmlElement(name = "phone")
    public List<Phone> getPhones() {
        return phones;
    }

    public Gender getGender() {
        return gender;
    }

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public String getEmail() {
        return email;
    }

    public String getIcq() {
        return icq;
    }

    public String getSkype() {
        return skype;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    @XmlTransient
    public String getPassword() {
        return password;
    }

    @XmlTransient
    public List<GroupRelation> getGroups() {
        return groups;
    }

    @XmlTransient
    public Role getRole() {
        return role;
    }

    //endregion

    //region Setters
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGroups(List<GroupRelation> groups) {
        this.groups = groups;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    //endregion

    //region Base methods
    @Override
    public String toString() {
        return "Account [id=" + id + ", email=" + email + " , password=" + password + ", name=" + name + ", surname=" +
                surName + ", middleName=" + middleName + ", gender=" + gender +
                ", phones=" + phones + ",  birthDate=" +
                birthDate + ",  homeAddress=" +
                homeAddress + ",  workAddress=" + workAddress + ", icq=" + icq + ", role=" + role +
                "]";
    }

    @Override
    public Account clone() {
        try {
            Account copy = (Account) super.clone();
            if (copy.getPhones() != null) {
                List<Phone> copyPhones = new ArrayList<>();
                for (int i = 0; i < copy.getPhones().size(); i++) {
                    copyPhones.add(copy.getPhones().get(i).clone());
                }
                this.phones = copyPhones;
            }
            return copy;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surName, middleName, gender, phones, birthDate, homeAddress, workAddress, email,
                icq, skype, additionalInfo, password);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Objects.equals(name, account.name) &&
                Objects.equals(surName, account.surName) &&
                Objects.equals(middleName, account.middleName) &&
                gender == account.gender &&
                Objects.equals(birthDate, account.birthDate) &&
                Objects.equals(homeAddress, account.homeAddress) &&
                Objects.equals(workAddress, account.workAddress) &&
                Objects.equals(email, account.email) &&
                Objects.equals(icq, account.icq) &&
                Objects.equals(skype, account.skype) &&
                Objects.equals(additionalInfo, account.additionalInfo) &&
                Objects.equals(password, account.password) &&
                Arrays.equals(photo, account.photo);
    }
    //endregion
}