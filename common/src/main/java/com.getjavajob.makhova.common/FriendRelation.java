package com.getjavajob.makhova.common;

import javax.persistence.*;
import java.util.Objects;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name = "relationship")
public class FriendRelation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_one_id")
    private Account accountOne;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_two_id")
    private Account accountTwo;
    @Enumerated(STRING)
    private FriendStatus status;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "action_id")
    private Account actionAccount;

    public FriendRelation() {
    }

    public FriendRelation(Account accountOne, Account accountTwo, FriendStatus status, Account actionAccount) {
        if (accountOne.getId() < accountTwo.getId()) {
            this.accountOne = accountOne;
            this.accountTwo = accountTwo;
        } else {
            this.accountOne = accountTwo;
            this.accountTwo = accountOne;
        }
        this.status = status;
        this.actionAccount = actionAccount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccountOne() {
        return accountOne;
    }

    public void setAccountOne(Account accountOne) {
        this.accountOne = accountOne;
    }

    public Account getAccountTwo() {
        return accountTwo;
    }

    public void setAccountTwo(Account accountTwo) {
        this.accountTwo = accountTwo;
    }

    public FriendStatus getStatus() {
        return status;
    }

    public void setStatus(FriendStatus status) {
        this.status = status;
    }

    public Account getActionAccount() {
        return actionAccount;
    }

    public void setActionAccount(Account actionAccount) {
        this.actionAccount = actionAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendRelation that = (FriendRelation) o;
        return id == that.id &&
                Objects.equals(accountOne, that.accountOne) &&
                Objects.equals(accountTwo, that.accountTwo) &&
                status == that.status &&
                Objects.equals(actionAccount, that.actionAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountOne, accountTwo, status, actionAccount);
    }

    @Override
    public String toString() {
        return "FriendRelation{" +
                "id=" + id +
                ", accountOne=" + getAccountOne() +
                ", accountTwo=" + accountTwo +
                ", status=" + status +
                '}';
    }
}