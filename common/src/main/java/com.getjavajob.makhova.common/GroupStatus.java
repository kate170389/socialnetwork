package com.getjavajob.makhova.common;

import javax.xml.bind.annotation.XmlEnumValue;

public enum GroupStatus {
    ADMIN(),
    REQUEST(),
    USER();

    public static GroupStatus findName(String name) {
        for (GroupStatus status : values()) {
            if (status.name().equals(name)) {
                return status;
            }
        }
        return null;
    }
}