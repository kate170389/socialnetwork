$(function () {

    $("#search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: contextPath + "/ajaxSearch",
                data: {
                    search: request.term
                },
                success: function (data) {
                    response($.map(data, function (obj, i) {
                        return {
                            value: request.term,
                            id: obj.id,
                            type: (Object.keys(obj).length == 15) ? "account" : "group",
                            label: (Object.keys(obj).length == 15) ?
                                "account: " + obj.name + ' ' + obj.surName : "group: " + obj.name
                        };
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            window.location.href = contextPath + '/' + ui.item.type + '/' + ui.item.id;
        }
    });

});