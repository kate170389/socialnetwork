$(function () {
    var stompClient = null;
    connect();

    function setConnected(connected) {
        document.getElementById('sendMessage').disabled = !connected;
    }

    function connect() {
        var socket = new SockJS(contextPath + '/room');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            setConnected(true);
            console.log('Connected: ' + frame);
            stompClient.subscribe('/user/queue/specific-user', function (messageOutput) {
                showMessageOutput(JSON.parse(messageOutput.body));
            });
        });
    }

    $('#sendMessage').click(function (e) {
        console.log('sendMessage()');
        var text = document.getElementById('text').value;
        var msg = {
            'accountFromEmail': from,
            'accountToEmail': to,
            'text': text,
            'accountFromId': fromId,
            'accountFromName': fromName
        };
        console.log(JSON.stringify(msg));
        stompClient.send("/app/room", {}, JSON.stringify(msg));
    });

    function showMessageOutput(messageOutput) {
        console.log('showMessageOutput: ' + messageOutput);
        $('#messages').prepend(
            '<div class="row justify-content-center">' +
            '<div class="col-md-12 order-1">' +
            '<div class="card-body col-12">' +
            '<div class="user-row">' +
            '<a href="' + contextPath + '/account/' + messageOutput.accountFromId + '">' +
            '<img src="' + contextPath + '/account/' + messageOutput.accountFromId + '/image"' +
            ' onerror="this.onerror=null;this.src=\'' + contextPath + '/resources/photo.jpg\';"' +
            ' width="50px" height="50px">' +
            messageOutput.accountFromName + ' (' + messageOutput.time + '):' +
            '</a>' +
            '</div>' +
            '<br>' +
            '<div class="shadow-textarea flex-fill">' + messageOutput.text +
            '</div>'
        )
    }
});