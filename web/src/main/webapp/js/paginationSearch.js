$(function () {
    var maxResult = 3;
    var accountsQty = $('#accountsQty').val();
    var groupsQty = $('#groupsQty').val();
    var lastAccountPageNumber = (Math.ceil(accountsQty / maxResult));
    var lastGroupsPageNumber = (Math.ceil(groupsQty / maxResult));

    getAccounts();
    getGroups();

    function getAccounts() {
        if (accountsQty == 0) {
            $("#accountsResult").append(
                '<h6 class="mb-3" align="center">not found</h6>'
            );
        } else {
            paginationAccounts();
        }
    }

    function getGroups() {
        if (groupsQty == 0) {
            $("#groupsResult").append(
                '<h6 class="mb-3" align="center">not found</h6>'
            );
        } else {
            paginationGroups();
        }
    }


    function paginationAccounts() {
        var result = $("#accountsResult");
        var url = contextPath + "/account/paginationSearch";
        var type = "account";
        $('#pagination-accounts').twbsPagination({
            totalPages: lastAccountPageNumber,
            visiblePages: 3,
            next: 'Next',
            prev: 'Prev',
            hideOnlyOnePage: true,
            onPageClick: function (event, page) {
                // result.load(urlLoading, searchAjax(page, result, url, type));
                //paginationsearch for account page-1 couse in twbsPagination the first 1, and in AccountRepository must be 0
                searchAjax(page - 1, result, url, type);
            }
        });
    }

    function paginationGroups() {
        var result = $("#groupsResult");
        var url = contextPath + "/group/paginationSearch";
        var type = "group";
        $('#pagination-groups').twbsPagination({
            totalPages: lastGroupsPageNumber,
            visiblePages: 3,
            next: 'Next',
            prev: 'Prev',
            hideOnlyOnePage: true,
            onPageClick: function (event, page) {
                // result.load(urlLoading, searchAjax(page, result, url, type));
                searchAjax(page, result, url, type);
            }
        });
    }

    function createAccountRow(dataArr, result) {
        var html = '';
        for (var i = 0; i < dataArr.length; i++) {
            var photo = "";
            if (dataArr[i].photo != null) {
                photo = contextPath + '/account/' + dataArr[i].id + '/image';
            } else {
                photo = contextPath + '/resources/photo.jpg';
            }
            html +=
                '<div class="user-row">' +
                '<a href="' + contextPath + '/account/' + dataArr[i].id + '">' +
                '<img src="' + photo + '" width="50px">' + ' ' +
                dataArr[i].name + ' ' + dataArr[i].surName + '</a>' +
                '</div>';
        }
        result.html(html);
    }

    function createGroupRow(dataArr, result) {
        var html = '';
        for (var i = 0; i < dataArr.length; i++) {
            var photo = "";
            if (dataArr[i].photo != null) {
                photo = contextPath + '/group/' + dataArr[i].id + '/image';
            } else {
                photo = contextPath + '/resources/photo.jpg';
            }
            html +=
                '<div class="user-row">' +
                '<a href="' + contextPath + '/group/' + dataArr[i].id + '">' +
                '<img src="' + photo + '" width="50px">' +
                dataArr[i].name + '</a>' +
                '</div>';
        }
        result.html(html);
    }

    function searchAjax(page, result, url, type) {
        $.ajax({
            url: url,
            type: "GET",
            data: {
                page: page,
                maxResult: maxResult,
                search: search
            },
            success: function (data) {
                console.log(data);
                if (type == 'account') {
                    createAccountRow(data, result);
                } else {
                    createGroupRow(data, result)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                // }
            },
            beforeSend: function () {
                result.html('<div id="wait" align="center">' +
                    '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' +
                    '<span class="sr-only">Loading...</span>' +
                    '</div>');
            }
        });
    }
});