$(function () {
    var index = $('.personalPhone').length + $('.workPhone').length;
    $('#addPersonalPhone').click(function (e) {
        var field1 = $('#newPersonalNumber');
        var number1 = field1.val();
        var regex1 = /\d{3}-\d{7}$/;
        var fieldError1 = $('#personalPhoneError');
        fieldError1.fadeOut();
        if (regex1.test(number1)) {
            $('#personalFields').append(
                '<div class="row mb-2 personalPhone">' +
                '<input type="hidden" name="phones[' + index + '].id" value="0">' +
                '<input type="hidden" name="phones[' + index + '].type" value="PERSONAL">' +
                '<div class="col-md-10">' +
                '<input type="text" class="form-control onePhone" id="personalNumber' + index + '" value="' +
                number1 + '" name="phones[' + index + '].number">' +
                '</div>' +
                '<div class="col-md-2" id="delete">' +
                '<button class="btn form-control deletePhone" type="button">' +
                'Delete' +
                '</button>' +
                '</div>' +
                '</div>'
            );
            index++;
            field1.val('');
        } else {
            fieldError1.fadeIn();
        }
    });

    $('#addWorkPhone').click(function (e) {
        var field = $('#newWorkNumber');
        var number = field.val();
        var regex = /\d{3}-\d{7}$/;
        var fieldError = $('#workPhoneError');
        fieldError.fadeOut();
        if (regex.test(number)) {
            $('#workFields').append(
                '<div class="row mb-2 workPhone">' +
                '<input type="hidden" name="phones[' + index + '].id" value="0">' +
                '<input type="hidden" name="phones[' + index + '].type" value="WORK">' +
                '<div class="col-md-10">' +
                '<input type="text" class="form-control onePhone" id="workNumber' + index + '" value="' +
                number + '" name="phones[' + index + '].number"> </div> ' +
                '<div class="col-md-2" id="delete">' +
                '<button class="btn form-control deletePhone" type="button">' +
                'Delete </button>' +
                '</div> ' +
                '</div>'
            );
            index++;
            field.val('');
        } else {
            fieldError.fadeIn();
        }
    });

    $('.deletePhone').click(function (e) {
        $(this).parent().parent().remove();
    });
});