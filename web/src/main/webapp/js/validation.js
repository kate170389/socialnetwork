$(function () {
    function validateInfo() {
        $('#submitInfo').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 45
                },
                surName: {
                    required: true,
                    minlength: 2,
                    maxlength: 45
                },
                middleName: {
                    minlength: 2,
                    maxlength: 45
                },
                email: {
                    required: true,
                    validEmail: true
                },
                password: {
                    required: false,
                    minlength: 5,
                    maxlength: 20
                },
                birthDate: {
                    required: true
                },
                workAddress: {
                    maxlength: 45
                },
                homeAddress: {
                    maxlength: 45
                },
                icq: {
                    validICQ: true
                },
                skype: {
                    maxlength: 45
                },
                additionalInfo: {
                    maxlength: 100
                }
            },
            messages: {
                name: {
                    required: "Please, enter your name",
                    minlength: "Your name must consist of at least 2 characters",
                    maxlength: "Your name must consist at most 45 characters"
                },
                surName: {
                    required: "Please, enter your surname",
                    minlength: "Your surname must consist of at least 2 characters",
                    maxlength: "Your surname must consist at most 45 characters"
                },
                middleName: {
                    minlength: "Your middle name must consist of at least 2 characters",
                    maxlength: "Your middle name must consist at most 45 characters"
                },
                email: {
                    required: "Please, enter your e-mail",
                    email: true
                },
                password: {
                    required: "Please, enter your password",
                    minlength: "Your password must consist of at least 5 characters",
                    maxlength: "Your password must consist at most 20 characters"
                },
                birthDate: {
                    required: "Please, enter your birth date"
                },
                workAddress: {
                    maxlength: "Your work address must consist at most 45 characters"
                },
                homeAddress: {
                    maxlength: "Your home address must consist at most 45 characters"
                },
                icq: {},
                skype: {
                    maxlength: "Your skype must consist at most 45 characters"
                },
                additionalInfo: {
                    maxlength: "Your additional info must consist at most 100 characters"
                }
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {
                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 100
                }, 1000);
            }
        });
        $.validator.addClassRules("onePhone", {
            phoneRequired: true,
            validPhone: true
        });
    }

    function validateGroup() {
        $('#submitGroupInfo').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 45
                },
                description: {
                    required: true,
                    minlength: 2,
                    maxlength: 100
                }
            },
            messages: {
                name: {
                    required: "Please, enter group name",
                    minlength: "Name must consist of at least 2 characters",
                    maxlength: "Name must consist at most 45 characters"
                },
                description: {
                    required: "Please, enter description",
                    minlength: "Description must consist of at least 2 characters",
                    maxlength: "Description must consist at most 100 characters"
                }
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {
                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 100
                }, 1000);
            }
        });
    }

    $.validator.setDefaults({
        errorClass: 'error-validation'
    });

    $.validator.addMethod("validEmail", function (value, element) {
        return this.optional(element) || /^([a-z0-9_.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/.test(value);
    }, "Your email must match the format");

    $.validator.addMethod("validICQ", function (value, element) {
        return this.optional(element) || /[0-9]{9}$/.test(value);
    }, "ICQ must contain 9 digits");

    $.validator.addMethod("phoneRequired", $.validator.methods.required,
        "Please, enter phone number");

    $.validator.addMethod("validPhone", function (value, element) {
        return this.optional(element) || /^[0-9]{3}-[0-9]{7}$/.test(value);
    }, "Phone number should be xxx-xxxxxxx");

    $('.saveChanges').click(function (e) {
        e.preventDefault();
        validateInfo();
        if ($('#submitInfo').valid()) {
            $('#myModal').modal('show');
        }
    });

    $('.submitGroup').click(function (e) {
        e.preventDefault();
        validateGroup();
        if ($('#submitGroupInfo').valid()) {
            $('#submitGroupInfo').submit();
        }
    });
});


//МОЖНО СДЕЛАТЬ ТАК:
// $('.onePersonalPhone').each(function () {
//     $(this).rules("add", {
//         required: true,
//         messages: {
//             required: "телефон не может быть пустым"
//         }
//     })
// });