$(function () {
    var showForm = false;

    $('#createMessage').click(function (e) {
        document.getElementById('messageForm').style.display = showForm ? 'none' : 'inline';
        showForm = !showForm;
    });
});