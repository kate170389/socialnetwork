$(function () {
    $('#birthDate').datepicker(
        {
            format: 'yyyy-mm-dd',
            locale: 'ru',
            autoclose: true
        });
});