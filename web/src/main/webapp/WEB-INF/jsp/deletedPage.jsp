<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="message" scope="request" type="java.lang.String"/>

<html>
<head>
    <title>DeletedPage</title>
    <%@include file="navbar.jsp" %>
</head>
<body>
<div class="text-center">
    <h4>${message}</h4>
</div>
</body>
</html>
