<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title>Serch Form</title>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/navbar.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>
<div class="navbar navbar-light " role="navigation" style="background-color: #e3f2fd;">
    <a class="navbar-brand"
       href="${pageContext.request.contextPath}/account/${sessionScope.account.id}">${sessionScope.account.name}
        ${sessionScope.account.surName}</a>
    <form class="form-inline" action="${pageContext.request.contextPath}/search">
        <input class="form-control mr-sm-2" id="search" type="search" name="search" placeholder="Search"
               aria-label="Search">
        <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
    </form>
    <form class="form-inline" action="${pageContext.request.contextPath}/logout">
        <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Logout</button>
    </form>
</div>

<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery/3.3.1/dist/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
<script>var contextPath = "${pageContext.request.contextPath}"</script>
<script src="${pageContext.request.contextPath}/js/autocomplete.js"></script>

</body>