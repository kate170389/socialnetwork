<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="account" scope="request" type="com.getjavajob.makhova.common.Account"/>
<jsp:useBean id="status" scope="request" type="java.lang.String"/>
<jsp:useBean id="relation" scope="request" type="java.lang.Integer"/>
<jsp:useBean id="requests" scope="request" type="java.lang.Integer"/>
<jsp:useBean id="messages" scope="request" type="java.util.List<com.getjavajob.makhova.common.Message>"/>

<html>
<head>
    <title>Account</title>
    <%@include file="../navbar.jsp" %>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/account.css" rel="stylesheet"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome.min.css"/>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="bord col-md-4 col-lg-4" align="center">
            <c:if test="${account.photo==null}">
                <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="200px">
            </c:if>
            <c:if test="${account.photo!=null}">
                <img src="${pageContext.request.contextPath}/account/${account.id}/image" width="200px">
            </c:if>
            <small class="form-text">
                <c:if test="${status=='admin'}">
                    <a href="${account.id}/update/photo">Update photo</a>
                </c:if>
            </small>
        </div>
        <div class="bord col-md-8 col-lg-8">
            <ul>
                <li>
                    ${account.surName} ${account.name} ${account.middleName}
                    <c:choose>
                        <c:when test="${status=='friend'}">
                            (this is your friend)
                        </c:when>
                        <c:when test="${status=='admin'}">
                            (this is your page)
                        </c:when>
                        <c:when test="${status=='sentRequest'}">
                            (you sent a friend request)
                        </c:when>
                        <c:when test="${status=='receivedRequest'}">
                            (this user has sent you a friend request)
                        </c:when>
                        <c:when test="${status=='sentDecline'}">
                            (you rejected a friend request)
                        </c:when>
                        <c:when test="${status=='receivedDecline'}">
                            (user rejected your friend request)
                        </c:when>
                        <c:when test="${status=='sentBlock'}">
                            (you blocked this user)
                        </c:when>
                        <c:when test="${status=='receivedBlock'}">
                            (user blocked you)
                        </c:when>
                    </c:choose>
                </li>
                <li>Gender: ${account.gender}</li>
                <li>Birth date: ${account.birthDate}</li>
                ICQ: ${account.icq}<br>
                Skype: ${account.skype}<br>
                <c:forEach var="phone" items="${account.phones}">
                    <c:choose>
                        <c:when test="${phone.type.name() eq 'WORK'}">
                            Work phone:
                        </c:when>
                        <c:when test="${phone.type.name() eq 'PERSONAL'}">
                            Personal phone:
                        </c:when>
                    </c:choose>
                    ${phone.number}<br>
                </c:forEach>
                <c:if test="${account.homeAddress!=null}">
                    Home address: ${account.homeAddress}<br>
                </c:if>
                Work address: ${account.workAddress}<br>
                ${account.additionalInfo}<br>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="href col-md-4 col-lg-4">
            <c:if test="${status=='admin'}">
                <a href="${account.id}/update/info"
                   class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-cogs" aria-hidden="true"></i> Edit account</a>

                <button class="btn btn-light btn-sm btn-block delete" type="button">
                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete account
                </button>

                <a href="${pageContext.request.contextPath}/group/create"
                   class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <i class="fa fa-users" aria-hidden="true"></i> Create group</a>
            </c:if>

            <c:if test="${pageContext.session.getAttribute('account').role=='ADMIN'}">
                <%--<c:if test="${status=='admin'|| pageContext.session.getAttribute('account').role=='ADMIN'}">--%>
                <button class=" btn btn-light btn-sm btn-block delete" type="button">
                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete account
                </button>
            </c:if>

            <c:if test="${status!='receivedBlock'}">
                <c:choose>
                    <c:when test="${status=='noStatus'}">
                        <a href="updateRelationship/sendRequest/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Send request</a>
                    </c:when>
                    <c:when test="${status=='friend'}">
                        <a href="updateRelationship/delete/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Delete from friends</a>
                    </c:when>
                    <c:when test="${status=='sentRequest'}">
                        <a href="updateRelationship/delete/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Decline request</a>
                    </c:when>
                    <c:when test="${status=='receivedRequest'}">
                        <a href="updateRelationship/accept/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Accept request</a>
                        <a href="updateRelationship/decline/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Decline request</a>
                    </c:when>
                    <c:when test="${status=='sentDecline'}">
                        <a href="updateRelationship/accept/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Accept request</a>
                    </c:when>
                    <c:when test="${status=='receivedDecline'}">
                        <a href="updateRelationship/delete/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Cancel request</a>
                    </c:when>
                    <c:when test="${status=='sentBlock'}">
                        <a href="updateRelationship/delete/${account.id}"
                           class="btn btn-light btn-sm btn-block">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i> Unlock user</a>
                    </c:when>
                </c:choose>

                <c:if test="${status!='admin' && status!='sentBlock'}">
                    <a href="updateRelationship/block/${account.id}"
                       class="btn btn-light btn-sm btn-block">
                        <i class="fa fa-user-times" aria-hidden="true"></i> Block user</a>
                </c:if>

                <a href="${account.id}/groups" class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-users" aria-hidden="true"></i> Groups</a>

                <a href="${account.id}/friends" class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-user" aria-hidden="true"></i> Friends
                    <c:if test="${status=='admin' && requests!=0}">
                        <span class="badge">${requests}</span>
                    </c:if></a>

                <a href="xmlConverter" class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-exchange" aria-hidden="true"></i> XML converter
                </a>

                <c:if test="${status=='friend'}">
                    <a href="${pageContext.request.contextPath}/chat/${account.id}"
                       class="btn btn-light btn-sm btn-block">
                        <i class="fa fa-comments-o" aria-hidden="true"></i> Send a message
                    </a>
                </c:if>
            </c:if>

            <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body text-center">
                            Are you sure you want to delete account?
                        </div>
                        <div class="modal-footer center-block">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="${account.id}/update/delete" class="btn btn-primary">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-lg-8" style="padding-top: 0px">
            <c:if test="${status=='admin' || status=='friend'}">
                <div class="row justify-content-center">
                    <div class="bord col-md-12 order-1">
                        <div class="card-body col-12 text-center" style="padding: 0px">
                            <button class="btn btn-info btn-sm m-2" type="button" id="createMessage">
                                new message
                            </button>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row justify-content-center" id="messageForm" style="display: none">
                <div class="col-md-12 order-1">
                    <form action="${pageContext.request.contextPath}/sendCommonMessage/${account.id}" method="post"
                          enctype="multipart/form-data">
                        <div class="card-body col-12">
                            <div class="row">
                                <div class="align-self-start" style="padding-bottom: 7px">
                                    <input type="file" name="file" id="file" placeholder="Add image">
                                </div>
                            </div>
                            <div class="row">
                                <div class="shadow-textarea flex-fill">
            <textarea class="form-control z-depth-1" name="text" id="text"
                      placeholder="Write a message" rows="3"></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" id="toId" name="toId"
                                   value="${account.id}">
                            <input type="hidden" class="form-control" id="type" name="type" value="COMMON">

                            <div class="row text-center">
                                <div class="col-12">                                <%--<div class="align-self-center" >--%>
                                    <input class="btn btn-info" id="sendMessage" type="submit"
                                           value="Send a message"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="messages">
                <c:forEach var="message" items="${messages}">
                    <div class="row justify-content-center">
                        <div class="bord col-md-12 order-1">
                            <div class="card-body col-12">
                                <div class="user-row">
                                    <a href="${pageContext.request.contextPath}/account/${message.accountFrom.id}">
                                        <img src="${pageContext.request.contextPath}/account/${message.accountFrom.id}/image"
                                             alt="Sorry! Image not available at this time"
                                             onError="this.onerror=null;
                                                     this.src='${pageContext.request.contextPath}/resources/photo.jpg';"
                                             width="25px" height="25px">
                                            ${message.accountFrom.name} ${message.accountFrom.surName} (
                                        <fmt:parseDate value="${message.creationDate}" pattern="yyyy-MM-dd'T'HH:mm"
                                                       var="parsedDateTime"/>
                                            <%--${parsedDateTime}--%>
                                        <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>):
                                    </a>
                                </div>
                                <br>
                                <c:if test="${message.photo!=null}">
                                    <img src="${pageContext.request.contextPath}/message/${message.id}/image"
                                         alt="Sorry! Image not available at this time"
                                         onError="this.onerror=null;
                                                 this.src='${pageContext.request.contextPath}/resources/photo.jpg';"
                                         style="max-width: inherit">
                                    <br>
                                </c:if>

                                <div class="shadow-textarea flex-fill">
                                        ${message.text}
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/delete.js"></script>
<script src="${pageContext.request.contextPath}/js/message.js"></script>
<script>
    var accountId = "${account.id}";
</script>
</body>
</html>