<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>xml</title>
    <%@include file="../navbar.jsp" %>
    <%--<link href="${pageContext.request.contextPath}/css/account.css" rel="stylesheet"/>--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/registration.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome.min.css"/>
</head>
<body>

<div class="container">
    <div class="row justify-content-center">
        <form action="xmlConverter/fromXml" method="post" enctype="multipart/form-data" class="col-md-5"
              style="padding: 0%">
            <div class="mb-3">
                <div class="row">
                    <div class="col-md-12" style="text-align: center">
                        <input type="file" name="fileXml" id="fileXml" placeholder="Chose file">
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="padding: 0%">
                <button class="btn btn-primary btn-lg btn-block saveChanges" type="submit">
                    Get account info from XML
                </button>
            </div>
        </form>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-5" style="padding: 0%">
            <a href="xmlConverter/toXml" class="btn btn-primary btn-lg btn-block saveChanges">
                <%--<button class="btn btn-primary btn-lg btn-block saveChanges" type="button">--%>
                Save account info to XML
                <%--</button>--%>
            </a>
        </div>
    </div>
</div>

</body>
</html>
