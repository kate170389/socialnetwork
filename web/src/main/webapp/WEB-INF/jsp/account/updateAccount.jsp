<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:useBean id="account" scope="request" type="com.getjavajob.makhova.common.Account"/>
<jsp:useBean id="personalPhones" scope="request" type="java.util.List<com.getjavajob.makhova.common.Phone>"/>
<jsp:useBean id="workPhones" scope="request" type="java.util.List<com.getjavajob.makhova.common.Phone>"/>
<html>
<head>
    <title>Update info</title>
    <%@include file="../navbar.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/calendar.css"/>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container theme-showcase" role="main">
    <div class="row justify-content-center">
        <div class="col-md-8 order-1">
            <h4 class="mb-3" align="center">Edit information</h4>

            <form id="submitInfo" action="${pageContext.request.contextPath}/account/${account.id}/update/info"
                  method="post" enctype="multipart/form-data">
                <input type="hidden" class="form-control" id="id" name="id" value="${account.id}">
                <%--<input type="hidden" class="form-control" id="role" name="role" value="${account.role}">--%>

                <div class="mb-3">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                           value="${account.name}" required>
                </div>

                <div class="mb-3">
                    <label for="surName">Last name:</label>
                    <input type="text" class="form-control" id="surName" name="surName" placeholder="Last name"
                           value="${account.surName}" required>
                </div>

                <div class="mb-3">
                    <label for="middleName">Middle name:</label>
                    <input type="text" class="form-control" id="middleName" name="middleName" placeholder="Middle name"
                           value="${account.middleName}">
                </div>

                <div class="mb-3">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" value="${account.email}"
                           placeholder="E-mail" required>
                    <small class="form-text text-danger">${requestScope.error}</small>
                </div>

                <div class="mb-3">
                    <label for="password"> Password:</label><br>
                    <input type="password" class="form-control" name="password" id="password" placeholder="*****">
                </div>

                <div class="d-block my-3">
                    Gender:<br>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" name="gender" id="male" value="MALE"
                        <c:if test="${account.gender=='MALE'}">
                               checked
                        </c:if> >
                        <label class="custom-control-label" for="male">Male</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" name="gender" id="female" value="FEMALE"
                        <c:if test="${account.gender=='FEMALE'}">
                               checked
                        </c:if>>
                        <label class="custom-control-label" for="female">Female</label>
                    </div>
                </div>

                <div class="mb-1" id="personalFields">
                    Personal phone numbers:<br>
                    <c:forEach var="phone" items="${personalPhones}" varStatus="personalIndex">
                        <input type="hidden" name="phones[${personalIndex.index}].id" value="${phone.id}">
                        <input type="hidden" name="phones[${personalIndex.index}].type" value="PERSONAL">
                        <div class="row mb-2 personalPhone">
                            <div class="col-md-10">
                                <input type="text" class="form-control onePhone"
                                       id="personalNumber${personalIndex.index}"
                                       name="phones[${personalIndex.index}].number"
                                       placeholder="${phone.number}" value="${phone.number}">
                            </div>
                            <div class="col-md-2">
                                <button class="btn form-control deletePhone" type="button">
                                    Delete
                                </button>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <div class="row mb-2" id="newPersonalPhone">
                    <div class="input-group">
                        <div class="input-group col-md-10">
                            <input type="text" class="form-control" id="newPersonalNumber" name="newPersonalNumber"
                                   placeholder="xxx-xxxxxxx" pattern="^\d{3}-\d{7}$">
                        </div>
                        <div class="col-md-2">
                            <button class="btn form-control btn-remove" type="button" id="addPersonalPhone">
                                Add
                            </button>
                        </div>
                    </div>
                </div>

                <div id="personalPhoneError" class="alert alert-danger collapse phone-error-msg" ontoggle="">
                    <strong>Error!</strong> Phone number should be xxx-xxxxxxx
                </div>

                <c:set var="count" value='${personalPhones.size()}'/>

                <div class="mb-1" id="workFields">
                    Work phone numbers:<br>
                    <c:forEach var="phone" items="${workPhones}" varStatus="workIndex">
                        <input type="hidden" name="phones[${workIndex.index+count}].id" value="${phone.id}">
                        <input type="hidden" name="phones[${workIndex.index+count}].type" value="WORK">
                        <div class="row mb-2 workPhone">
                            <div class="col-md-10">
                                <input type="text" class="form-control onePhone" id="workNumber${workIndex.index+count}"
                                       name="phones[${workIndex.index+count}].number" placeholder="${phone.number}"
                                       value="${phone.number}">
                            </div>
                            <div class="col-md-2">
                                <button class="btn form-control deletePhone" type="button">
                                    Delete
                                </button>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <div class="row mb-2" id="newWorkPhone">
                    <div class="input-group">
                        <div class="input-group col-md-10">
                            <input type="text" class="form-control" id="newWorkNumber" name="newWorkNumber"
                                   placeholder="xxx-xxxxxxx" pattern="^\d{3}-\d{7}$">
                        </div>
                        <div class="col-md-2">
                            <button class="btn form-control btn-remove" type="button" id="addWorkPhone">
                                Add
                            </button>
                        </div>
                    </div>
                </div>

                <div id="workPhoneError" class="alert alert-danger collapse phone-error-msg" ontoggle="">
                    <strong>Error!</strong> Phone number should be xxx-xxxxxxx
                </div>

                <div class="mb-3">
                    <label for="birthDate">Birth date:</label>
                    <input type="text" class="form-control " id="birthDate" name="accountBirthDate"
                           placeholder="Birth date"
                           value="${account.birthDate}" required>
                </div>

                <div class="mb-3">
                    <label for="workAddress">Work address:</label>
                    <input type="text" class="form-control" id="workAddress" name="workAddress"
                           placeholder="Work address" value="${account.workAddress}">
                </div>

                <div class="mb-3">
                    <label for="homeAddress">Home address:</label>
                    <input type="text" class="form-control" id="homeAddress" name="homeAddress"
                           placeholder="Home address" value="${account.homeAddress}">
                </div>

                <div class="mb-3">
                    <label for="icq">ICQ:</label>
                    <input type="text" class="form-control" id="icq" name="icq" placeholder="ICQ" value="${account.icq}"
                           pattern="[0-9]{9}">
                </div>

                <div class="mb-3">
                    <label for="skype">Skype:</label>
                    <input type="text" class="form-control" id="skype" name="skype" placeholder="Skype"
                           value="${account.skype}">
                </div>

                <div class="mb-3">
                    <label for="additionalInfo">Additional info:</label>
                    <input type="text" class="form-control" id="additionalInfo" name="additionalInfo"
                           placeholder="Additional info" value="${account.additionalInfo}">
                </div>

                <%--<!-- Button trigger modal -->--%>
                <button class="btn btn-primary btn-lg btn-block saveChanges" type="button">
                    Save changes
                </button>

                <%--<!-- Modal -->--%>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <%--<h4 class="modal-title" id="myModalLabel">Название модали</h4>--%>
                            </div>
                            <div class="modal-body text-center">
                                Save changes?
                            </div>
                            <div class="modal-footer center-block">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-validation/1.17.0/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-validation/1.17.0/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/js/updatePhones.js"></script>
<script src="${pageContext.request.contextPath}/js/calendar.js"></script>
<script src="${pageContext.request.contextPath}/js/validation.js"></script>
</body>
</html>