<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="status" scope="request" type="java.lang.String"/>
<jsp:useBean id="friends" scope="request" type="java.util.List<com.getjavajob.makhova.common.Account>"/>
<jsp:useBean id="requests" scope="request" type="java.util.List<com.getjavajob.makhova.common.Account>"/>
<jsp:useBean id="blocked" scope="request" type="java.util.List<com.getjavajob.makhova.common.Account>"/>
<jsp:useBean id="account" scope="request" type="com.getjavajob.makhova.common.Account"/>
<html>
<head>
    <title>${account.name} ${account.surName} friends</title>
    <%@include file="../navbar.jsp" %>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/search.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <c:if test="${status=='admin'}">
        <c:if test="${fn:length(requests)>0}">
            <div class="row justify-content-center">
                <div class="col-md-8 order-1">
                    <h5 class="mb-3">Requests:</h5>
                    <c:forEach items="${requests}" var="user">
                        <div class="row user-row">
                            <div class="col-md-6 col-lg-6 ">
                                <a class="user"
                                   href="${pageContext.request.contextPath}/account/${user.id}">
                                    <c:if test="${user.photo==null}">
                                        <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px"
                                             height="50px">
                                    </c:if>
                                    <c:if test="${user.photo!=null}">
                                        <img src="${pageContext.request.contextPath}/account/${user.id}/image"
                                             width="50px" height="50px">
                                    </c:if>
                                        ${user.name} ${user.surName}
                                </a>
                            </div>
                            <div class="col-md-3 col-lg-3 ">
                                <a href="${pageContext.request.contextPath}/account/updateRelationship/accept/${user.id}"
                                   class="btn btn-primary btn-sm small"> accept </a>
                            </div>
                            <div class="col-md-3 col-lg-3 ">
                                <a href="${pageContext.request.contextPath}/account/updateRelationship/decline/${user.id}"
                                   class="btn btn-primary btn-sm small"> decline </a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <br>
        </c:if>

        <c:if test="${fn:length(blocked)>0}">
            <div class="row justify-content-center">
                <div class="col-md-8 order-1">
                    <h5 class="mb-3">Blocked users:</h5>
                    <c:forEach items="${blocked}" var="user">
                        <div class="row user-row">
                            <div class="col-md-9 col-lg-9 ">
                                <a class="user" href="${pageContext.request.contextPath}/account/${user.id}">
                                    <c:if test="${user.photo==null}">
                                        <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px"
                                             height="50px">
                                    </c:if>
                                    <c:if test="${user.photo!=null}">
                                        <img src="${pageContext.request.contextPath}/account/${user.id}/image"
                                             width="50px" height="50px">
                                    </c:if>
                                        ${user.name} ${user.surName}
                                </a>
                            </div>
                            <div class="col-md-3 col-lg-3 justify-content-center">
                                <a href="${pageContext.request.contextPath}/account/updateRelationship/delete/
                                ${user.id}" class="btn btn-primary btn-sm small"> unlock </a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <br>
        </c:if>
    </c:if>

    <div class="row justify-content-center">
        <div class="col-md-8 order-1">
            <h5 class="mb-3">Friends:</h5>
            <c:if test="${fn:length(friends)>0}">
                <c:forEach items="${friends}" var="friend">
                    <div class="row user-row">
                        <div class="col-md-9 col-lg-9 ">
                            <a class="user" href="${pageContext.request.contextPath}/account/${friend.id}">
                                <c:if test="${friend.photo==null}">
                                    <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px"
                                         height="50px">
                                </c:if>
                                <c:if test="${friend.photo!=null}">
                                    <img src="${pageContext.request.contextPath}/account/${friend.id}/image"
                                         width="50px" height="50px">
                                </c:if>
                                    ${friend.name} ${friend.surName}
                            </a>
                        </div>
                        <c:if test="${status=='admin'}">
                            <div class="col-md-3 col-lg-3 ">
                                <a href="${pageContext.request.contextPath}/account/updateRelationship/delete/
                                ${friend.id}"
                                   class="btn btn-primary btn-sm small"> delete </a>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>