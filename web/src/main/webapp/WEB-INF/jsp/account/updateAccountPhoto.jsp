<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="account" scope="request" type="com.getjavajob.makhova.common.Account"/>
<html>
<head>
    <title>Update photo</title>
    <%@include file="../navbar.jsp" %>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 order-1">
            <h4 class="mb-3" align="center">Choose new photo</h4>
            <form action="photo" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <input type="file" name="file" id="file" placeholder="Your photo">
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Submit photo</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>