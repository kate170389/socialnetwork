<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="relation" scope="request" type="java.lang.String"/>
<jsp:useBean id="adminGroups" scope="request" type="java.util.List<com.getjavajob.makhova.common.Group>"/>
<jsp:useBean id="groups" scope="request" type="java.util.List<com.getjavajob.makhova.common.Group>"/>
<html>
<head>
    <title>Account groups</title>
    <%@include file="../navbar.jsp" %>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/search.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <c:if test="${relation=='admin' && fn:length(adminGroups)>0}">
        <div class="row justify-content-center">
            <div class="col-md-8 order-1">
                <h5 class="mb-3">My groups:</h5>
                <c:forEach items="${adminGroups}" var="group">
                    <div class="row user-row">
                        <a class="groupRelation"
                           href="${pageContext.request.contextPath}/group/${group.id}">
                            <c:if test="${group.photo==null}">
                                <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px">
                            </c:if>
                            <c:if test="${group.photo!=null}">
                                <img src="${pageContext.request.contextPath}/group/${group.id}/image"
                                     width="50px">
                            </c:if>
                                ${group.name}
                        </a>
                    </div>
                </c:forEach>
            </div>
        </div>
        <br>
    </c:if>
    <div class="row justify-content-center">
        <div class="col-md-8 order-1">
            <h5 class="mb-3">Groups:</h5>
            <c:forEach items="${groups}" var="group">
                <div class="row user-row">
                    <a class="groupRelation" href="${pageContext.request.contextPath}/group/${group.id}">
                        <c:if test="${group.photo==null}">
                            <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px">
                        </c:if>
                        <c:if test="${group.photo!=null}">
                            <img src="${pageContext.request.contextPath}/group/${group.id}/image"
                                 width="50px">
                        </c:if>
                            ${group.name}
                    </a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>