<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login form </title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/login.css"/>
</head>
<body>
<h1> Login</h1>
<div>
    <form action="${pageContext.request.contextPath}/login" method="post">
        <label for="username">Email:</label>
        <input type="username" id="username" name="username" placeholder="Your email">
        <label for="password"> Password:</label><br>
        <input type="password" name="password" id="password" placeholder="Your password">
        <br>
        <label for="remember-me">Remember me</label>
        <input type="checkbox" id="remember-me" name="remember-me" value="true"><br><br>
        <input type="submit" value="Login">
    </form>
    <a href="registration"> Registration</a>
</div>

<c:if test="${param.error}">
    <p class="error">
        Invalid username or password.
    </p>
</c:if>

<c:if test="${param.logout}">
    <p class="error">
        You have been logged out.
    </p>
</c:if>
</body>
</html>