<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="exception" scope="request" type="java.lang.Exception"/>
<jsp:useBean id="url" scope="request" type="java.lang.StringBuffer"/>


<html>
<head>
    <title>Error</title>
    <%@include file="navbar.jsp" %>
</head>
<body>
<div class="text-center">
    <h3>Error!</h3>
    <h4>${exception.message}</h4>
    <h4>Status code: ${status}</h4>
    <h4>URL: ${url}</h4>
    <button type="button" class="btn btn-default" name="back" onclick="history.back()">Go back</button>
</div>
</body>
</html>