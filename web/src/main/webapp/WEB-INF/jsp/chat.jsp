<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="messages" scope="request" type="java.util.List<com.getjavajob.makhova.common.Message>"/>
<jsp:useBean id="interlocutor" scope="request" type="com.getjavajob.makhova.common.Account"/>

<html>
<head>
    <title>Chat WebSocket</title>
    <%@include file="navbar.jsp" %>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/account.css" rel="stylesheet"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome.min.css"/>
</head>

<body>
<div class="container">

    <div class="row justify-content-center">
        <div class="bord col-md-12 order-1">
            <div class="card-body col-12">
                <div class="shadow-textarea flex-fill">
                    <textarea class="form-control z-depth-1" name="personalMessageText" id="text"
                              placeholder="Write a message" rows="3"></textarea>
                </div>
                <div class="align-self-center">
                    <input class="btn btn-info btn-sm m-2" id="sendMessage" type="submit" value="Send"/>
                </div>
            </div>
        </div>
    </div>

    <div id="messages">
        <c:forEach var="message" items="${messages}">
            <div class="row justify-content-center">
                <div class="bord col-md-12 order-1">
                    <div class="card-body col-12">
                        <div class="user-row">
                            <a href="${pageContext.request.contextPath}/account/${message.accountFrom.id}">
                                <img src="${pageContext.request.contextPath}/account/${message.accountFrom.id}/image"
                                     alt="Sorry! Image not available at this time"
                                     onError="this.onerror=null;
                                             this.src='${pageContext.request.contextPath}/resources/photo.jpg';"
                                     width="50px" height="50px">
                                    ${message.accountFrom.name} ${message.accountFrom.surName} (
                                <fmt:parseDate value="${message.creationDate}" pattern="yyyy-MM-dd'T'HH:mm"
                                               var="parsedDateTime"/>
                                <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>):
                            </a>
                        </div>
                        <br>
                        <div class="shadow-textarea flex-fill">
                                ${message.text}
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.3.0/sockjs.min.js"></script>--%>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>--%>
    <script src="${pageContext.request.contextPath}/webjars/sockjs-client/dist/sockjs.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/chat.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/stomp-websocket/2.3.3/stomp.js"></script>
    <script>
        var contextPath = "${pageContext.request.contextPath}";
        var from = "${pageContext.session.getAttribute('account').email}";
        var to = "${interlocutor.email}";
        var fromId = "${pageContext.session.getAttribute('account').id}";
        var fromName = "${pageContext.session.getAttribute('account').name} ${pageContext.session.getAttribute('account').surName}";
    </script>
</body>
</html>
