<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<jsp:useBean id="group" scope="request" type="com.getjavajob.makhova.common.Group"/>--%>
<html>
<head>
    <title>Create group</title>
    <%@include file="../navbar.jsp" %>
</head>
<body>
<div class="container theme-showcase" role="main">
    <div class="row justify-content-center">
        <div class="col-md-8 order-1">
            <h4 class="mb-3" align="center">Group creation</h4>
            <form action="create" id="submitGroupInfo" method="post" enctype="multipart/form-data">

                <div class="mb-3 align-content-center">
                    <label for="name">Group name:</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Group name" required>
                </div>

                <div class="mb-3">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" id="description" name="description"
                           placeholder="Description" required>
                </div>

                <div class="mb-3">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="m-md-3" for="file">Photo:</label>
                        </div>
                        <div class="col-md-5">
                            <input type="file" name="file" id="file" placeholder="Group photo">
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary btn-lg btn-block submitGroup" type="submit">Create new group</button>
            </form>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-validation/1.17.0/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-validation/1.17.0/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/js/validation.js"></script>
</body>
</html>
