<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="group" scope="request" type="com.getjavajob.makhova.common.Group"/>
<jsp:useBean id="relation" scope="request" type="java.lang.String"/>
<jsp:useBean id="requests" scope="request" type="java.util.List<com.getjavajob.makhova.common.Account>"/>
<jsp:useBean id="admins" scope="request" type="java.util.List<com.getjavajob.makhova.common.Account>"/>
<jsp:useBean id="users" scope="request" type="java.util.List<com.getjavajob.makhova.common.Account>"/>

<html>
<head>
    <title>Group members</title>
    <%@include file="../navbar.jsp" %>
    <link href="webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/search.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <h4 class="mb-3" align="center">Group ${group.name}</h4>
    </div>

    <c:if test="${fn:length(admins)>0}">
        <div class="row justify-content-center">
            <div class="col-md-8 order-1">
                <h5 class="mb-3">Admins:</h5>
                <c:forEach items="${admins}" var="user">
                    <div class="row user-row">
                        <div class="col-md-8 col-lg-8 ">
                            <a class="user" href="${pageContext.request.contextPath}/account/${user.id}">
                                <c:if test="${user.photo==null}">
                                    <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px">
                                </c:if>
                                <c:if test="${user.photo!=null}">
                                    <img src="${pageContext.request.contextPath}/account/${user.id}/image"
                                         width="50px">
                                </c:if>
                                    ${user.name} ${user.surName}
                            </a>
                        </div>
                        <c:if test="${relation=='admin'}">
                            <div class=" col-md-4 col-lg-4 ">
                                <a href="updateRelation/makeUser/${user.id}"
                                   class="btn btn-primary btn-sm big"> make user </a>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
            </div>
        </div>
    </c:if>
    <br>
    <c:if test="${relation=='admin' && fn:length(requests)>0}">
        <div class="row justify-content-center">
            <div class="col-md-8 order-1">
                <h5 class="mb-3">Requests:</h5>
                <c:forEach items="${requests}" var="user">
                    <div class="row user-row">
                        <div class="col-md-8 col-lg-8 ">
                            <a class="user" href="${pageContext.request.contextPath}/account/${user.id}">
                                <c:if test="${user.photo==null}">
                                    <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px">
                                </c:if>
                                <c:if test="${user.photo!=null}">
                                    <img src="${pageContext.request.contextPath}/account/${user.id}/image"
                                         width="50px">
                                </c:if>
                                    ${user.name} ${user.surName}
                            </a>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <a href="updateRelation/makeUser/${user.id}"
                               class="btn btn-primary btn-sm big"> accept </a>
                        </div>
                        <div class="col-md-2 col-lg-2 ">
                            <a href="updateRelation/deleteRelation/${user.id}"
                               class="btn btn-primary btn-sm big"> decline </a>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </c:if>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8 order-1">
            <h5 class="mb-3">Users:</h5>
            <c:forEach items="${users}" var="user">
                <div class="row user-row">
                    <div class="col-md-8 col-lg-8 ">
                        <a class="user" href="${pageContext.request.contextPath}/account/${user.id}">
                            <c:if test="${user.photo==null}">
                                <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="50px">
                            </c:if>
                            <c:if test="${user.photo!=null}">
                                <img src="${pageContext.request.contextPath}/account/${user.id}/image"
                                     width="50px">
                            </c:if>
                                ${user.name} ${user.surName}
                        </a>
                    </div>

                    <c:if test="${relation=='admin'}">
                        <div class="col-md-2 col-lg-2">
                            <a href="updateRelation/deleteRelation/${user.id}"
                               class="btn btn-primary btn-sm big"> delete </a>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <a href="updateRelation/makeAdmin/${user.id}"
                               class="btn btn-primary btn-sm big"> admin </a>
                        </div>
                    </c:if>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>