<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="group" scope="request" type="com.getjavajob.makhova.common.Group"/>
<jsp:useBean id="relation" scope="request" type="java.lang.String"/>
<jsp:useBean id="requests" scope="request" type="java.lang.Integer"/>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Group</title>
    <%@include file="../navbar.jsp" %>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/group.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-lg-4" align="center">
            <c:if test="${group.photo==null}">
                <img src="${pageContext.request.contextPath}/resources/photo.jpg" width="200px">
            </c:if>
            <c:if test="${group.photo!=null}">
                <img src="${pageContext.request.contextPath}/group/${group.id}/image" width="200px">
            </c:if>
        </div>
        <div class="col-md-8 col-lg-8">
            <ul>
                <li>
                    Group name: ${group.name}
                    <c:if test="${relation=='ADMIN'}"> (you are the administrator)</c:if>
                    <c:if test="${relation=='USER'}"> (you are the member of the group)</c:if>
                    <c:if test="${relation=='REQUEST'}"> (request has been sent)</c:if>
                    <c:if test="${relation=='NON'}"> (you are not a member)</c:if>
                </li>
                <li>Date of creation: ${group.dateOfCreation}</li>
                <li>Description: ${group.description}</li>
            </ul>
        </div>
    </div>
    <c:set var="account" value='${pageContext.session.getAttribute("account")}'/>
    <div class="row">
        <div class="href col-md-4 col-lg-4">
            <c:if test="${relation=='ADMIN'}">
                <a href="${group.id}/update/info"
                   class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-cogs" aria-hidden="true"></i> Edit group</a>

                <button
                        class="btn btn-light btn-sm btn-block delete" type="button">
                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete group
                </button>
            </c:if>

            <a href="${group.id}/members" class="btn btn-light btn-sm btn-block">
                <i class="fa fa-user" aria-hidden="true"></i> Members
                <c:if test="${relation=='ADMIN' && requests!=0}">
                    <span class="badge">${requests}</span>
                </c:if></a>

            <c:set var="account" value='${sessionScope.account}'/>

            <c:if test="${relation=='USER'}">
                <a href="${group.id}/updateRelation/deleteRelation/${account.id}"
                   class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-user-times" aria-hidden="true"></i> Leave the group</a>
            </c:if>
            <c:if test="${relation=='REQUEST'}">
                <a href="${group.id}/updateRelation/deleteRelation/${account.id}"
                   class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-user-times" aria-hidden="true"></i> Cancel request</a>
            </c:if>
            <c:if test="${relation=='NON'}">
                <a href="${group.id}/updateRelation/sendRequest/${account.id}"
                   class="btn btn-light btn-sm btn-block">
                    <i class="fa fa-user-plus" aria-hidden="true"></i> Send request
                </a>
            </c:if>
        </div>
        <div class="col-md-8 col-lg-8" align="center">
        </div>
    </div>

    <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body text-center">
                    Are you sure you want to delete the group?
                </div>
                <div class="modal-footer center-block">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="${group.id}/update/delete" class="btn btn-primary">Delete</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/delete.js"></script>
</body>
</html>