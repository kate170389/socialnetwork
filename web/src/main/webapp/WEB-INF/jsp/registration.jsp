<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/calendar.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/registration.css"/>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container theme-showcase" role="main">
    <div class="row justify-content-center">
        <div class="col-md-8 order-1">
            <h4 class="mb-3" align="center">Registration</h4>
            <form action="registration" id="submitRegistration" method="post" enctype="multipart/form-data">
                <div class="mb-3 align-content-center">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                </div>

                <div class="mb-3">
                    <label for="surName">Last name:</label>
                    <input type="text" class="form-control" id="surName" name="surName" placeholder="Last Name"
                           required>
                </div>

                <div class="mb-3">
                    <label for="middleName">Middle name:</label>
                    <input type="text" class="form-control" id="middleName" name="middleName" placeholder="Middle Name">
                </div>

                <div class="mb-3">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required>
                    <small class="form-text text-danger">${requestScope.error}</small>
                </div>

                <div class="mb-3">
                    <label for="passport"> Pasword:</label><br>
                    <input type="password" class="form-control" name="password" id="passport"
                           placeholder="Your password"
                           required>
                </div>

                <div class="d-block my-3">
                    Gender:<br>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" name="gender" id="male" value="MALE" checked
                               required>
                        <label class="custom-control-label" for="male">Male</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" name="gender" id="female" value="FEMALE"
                               required>
                        <label class="custom-control-label" for="female">Female</label>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="birthDate">Birth date:</label>
                    <input type="text" class="form-control " id="birthDate" name="birthDate" placeholder="Birth date"
                           required>
                </div>

                <div class="mb-3">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="file">Photo:</label>
                        </div>
                        <div class="col-md-5">
                            <input type="file" name="file" id="file" placeholder="Your photo">
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to registration</button>

            </form>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/webjars/jquery/3.3.1/dist/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-validation/1.17.0/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-validation/1.17.0/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/js/calendar.js"></script>
<script src="${pageContext.request.contextPath}/js/validation.js"></script>
</body>
</html>