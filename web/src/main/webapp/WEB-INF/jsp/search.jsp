<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Search</title>
    <%@include file="navbar.jsp" %>
    <link href="${pageContext.request.contextPath}/css/account.css" rel="stylesheet"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome.min.css"/>
</head>
<body>
<div class="container">
    <input type="hidden" id="accountsQty" value="${requestScope.accounts}">
    <input type="hidden" id="groupsQty" value="${requestScope.groups}">
    <input type="hidden" id="search" value="${requestScope.pattern}">

    <div class="row justify-content-center">
        <div class="col-md-5 order-1">
            <div class="card-body" style="min-height: 230px">
                <h5 class="mb-3" align="center">Accounts:</h5>
                <div id="accountsResult"></div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <ul id="pagination-accounts" class="pagination-sm"></ul>
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-5 order-1">
            <div class="card-body" style="min-height: 230px">
                <h5 class="mb-3" align="center">Groups:</h5>
                <div id="groupsResult"></div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <ul id="pagination-groups" class="pagination-sm"></ul>
    </div>
</div>

<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/twbs-pagination/1.4.1/jquery.twbsPagination.min.js"></script>
<script>var search = "${requestScope.pattern}"</script>
<script src="${pageContext.request.contextPath}/js/paginationSearch.js"></script>

</body>
</html>