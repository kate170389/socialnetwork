package com.getjavajob.makhova.web;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.common.Message;
import com.getjavajob.makhova.common.MessageType;
import com.getjavajob.makhova.service.AccountService;
import com.getjavajob.makhova.web.utils.ChatMessage;
import com.getjavajob.makhova.web.utils.CommonMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(ChatMessage.class);

    private SimpMessagingTemplate simpMessagingTemplate;
    private AccountService accountService;

    @Autowired
    public MessageController(SimpMessagingTemplate simpMessagingTemplate, AccountService accountService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.accountService = accountService;
    }

    @MessageMapping("/room")
    public void send(ChatMessage chatMessage, Principal principal) throws Exception {
        logger.info("GET request to /room");
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
        chatMessage.setTime(time);
        Message message = new Message(
                accountService.get(principal.getName()),
                accountService.get(chatMessage.getAccountToEmail()),
                chatMessage.getText(),
                null,
                MessageType.PERSONAL,
                LocalDateTime.now());
        accountService.createMessage(message);
        String to = chatMessage.getAccountToEmail();
        simpMessagingTemplate.convertAndSendToUser(to, "/queue/specific-user", chatMessage);
        simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/specific-user", chatMessage);
    }

    @GetMapping("/chat/{accountId}")
    public ModelAndView getChat(@PathVariable int accountId,
                                @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to /chat between accounts with id={} and id={}", accountId, sessionAccount.getId());
        List<Message> messages = accountService.getMessages(MessageType.PERSONAL, sessionAccount.getId(), accountId);
        Account interlocutor = accountService.get(accountId);
        ModelAndView modelAndView = new ModelAndView("chat");
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("interlocutor", interlocutor);
        return modelAndView;
    }

    @PostMapping("/sendCommonMessage/{accountToId}")
    public ModelAndView sendMessage(@ModelAttribute CommonMessage commonMessage,
                                    @PathVariable int accountToId,
                                    @RequestParam("file") MultipartFile file,
                                    @SessionAttribute("account") Account sessionAccount,
                                    HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("GET request to send message to account with id={}.", accountToId);
        byte[] photo = file.isEmpty() ? null : file.getBytes();
        Message message = new Message(
                sessionAccount,
                accountService.get(accountToId),
                commonMessage.getText(),
                photo,
                commonMessage.getType(),
                LocalDateTime.now()
        );
        accountService.createMessage(message);
        return new ModelAndView("redirect:/account/" + accountToId);
    }

    @RequestMapping(value = "message/{messageId}/image", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getImage(@PathVariable int messageId, @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to show message image with id={}.", messageId);
        return accountService.getMessage(messageId).getPhoto();
    }
}