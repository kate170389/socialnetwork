package com.getjavajob.makhova.web.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookiesUtil {
    private final static int AGE_COOKIE = 60 * 60 * 24 * 14;

    public static void setCookie(String name, String value, HttpServletResponse resp, HttpServletRequest req) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath(req.getContextPath());
        cookie.setMaxAge(AGE_COOKIE);
        resp.addCookie(cookie);
    }

    public static void replyCookie(Cookie cookie, String newValue, HttpServletResponse resp, HttpServletRequest req) {
        if (cookie != null) {
            cookie.setPath(req.getContextPath());
            cookie.setMaxAge(AGE_COOKIE);
            cookie.setValue(newValue);
            resp.addCookie(cookie);
        }
    }

    public static void deleteCookie(Cookie cookie, HttpServletResponse resp) {
        if (cookie != null) {
            cookie.setMaxAge(0);
            resp.addCookie(cookie);
        }
    }
}