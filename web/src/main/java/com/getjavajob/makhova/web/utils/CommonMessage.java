package com.getjavajob.makhova.web.utils;

import com.getjavajob.makhova.common.MessageType;

public class CommonMessage {
    //    private int accountFromId;
    private int toId;
    private String text;
    private MessageType type;

    CommonMessage() {
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }
}
