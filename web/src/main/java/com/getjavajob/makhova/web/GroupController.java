package com.getjavajob.makhova.web;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.common.Group;
import com.getjavajob.makhova.common.GroupRelation;
import com.getjavajob.makhova.service.GroupService;
import com.getjavajob.makhova.web.utils.UIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/group")
public class GroupController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("/{groupId}")
    public ModelAndView showGroup(@PathVariable int groupId,
                                  @SessionAttribute("account") Account accountSession) {
        logger.info("GET request to show group page with id={}.", groupId);
        Group group = groupService.get(groupId);
        GroupRelation relation = groupService.getRelation(accountSession.getId(), groupId);
        String status = (relation == null ? "NON" : relation.getStatus().toString());
        ModelAndView modelAndView = new ModelAndView("group/group");
        modelAndView.addObject("group", group);
        modelAndView.addObject("relation", status);
        modelAndView.addObject("requests", groupService.getRequests(group.getId()).size());
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView showGroupCreate() {
        logger.info("GET request to page create new group.");
        return new ModelAndView("group/createGroup");
    }

    @PostMapping("/create")
    public String doGroupCreate(@ModelAttribute Group group,
                                @RequestParam("file") MultipartFile file,
                                @SessionAttribute("account") Account sessionAccount) throws IOException {
        logger.info("POST request to create new group.");
        group.setDateOfCreation(LocalDate.now());
        if (!file.isEmpty()) {
            byte[] photo = file.getBytes();
            group.setPhoto(photo);
        }
        groupService.createGroup(sessionAccount, group);
        return "redirect:/group/" + group.getId();
    }

    @GetMapping("/{groupId}/update/{action}")
    public ModelAndView showUpdateGroup(@PathVariable int groupId,
                                        @PathVariable String action,
                                        @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to update group.");
        Group group = groupService.get(groupId);
        if (!groupService.isAdmin(sessionAccount, group)) {
            throw new UIException("You cannot make changes (account with id=" + sessionAccount.getId() +
                    " is not admin of group with id=" + group.getName() + ")");
        }
        ModelAndView modelAndView;
        switch (action) {
            case "info":
                modelAndView = new ModelAndView("group/updateGroup");
                modelAndView.addObject("group", group);
                break;
            case "delete":
                modelAndView = new ModelAndView("deletedPage");
                modelAndView.addObject("message", "The group " + group.getName() + " has been deleted.");
                groupService.delete(group, sessionAccount);
                break;
            default:
                modelAndView = new ModelAndView("redirect:/group/" + group.getId());
                break;
        }
        return modelAndView;
    }

    @PostMapping("/{groupId}/update/info")
    public String doUpdateGroup(@ModelAttribute Group group,
                                @RequestParam("file") MultipartFile file,
                                @SessionAttribute("account") Account sessionAccount) throws IOException {
        logger.info("POST request to update group info.");
        if (!groupService.isAdmin(sessionAccount, group)) {
            throw new UIException("You cannot make changes (account with id=" + sessionAccount.getId() +
                    " is not admin of group with id=" + group.getName() + ")");
        }
        Group oldGroup = groupService.get(group.getId());
        group.setDateOfCreation(oldGroup.getDateOfCreation());
        if (!file.isEmpty()) {
            byte[] photo = file.getBytes();
            group.setPhoto(photo);
        } else {
            group.setPhoto(oldGroup.getPhoto());
        }
        groupService.update(group, sessionAccount);
        return "redirect:/group/" + group.getId();
    }

    @GetMapping("/{groupId}/members")
    public ModelAndView showFriends(@PathVariable int groupId,
                                    @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to show members of group with id={}.", groupId);
        Group group = groupService.get(groupId);
        ModelAndView modelAndView = new ModelAndView("group/groupMembers");
        modelAndView.addObject("relation", (groupService.isAdmin(sessionAccount, group) ? "admin" : "notAdmin"));
        List<Account> admins = groupService.getAdmins(groupId);
        admins.remove(sessionAccount);
        List<Account> users = groupService.getUsers(groupId);
        List<Account> requests = groupService.getRequests(groupId);
        modelAndView.addObject("admins", admins);
        modelAndView.addObject("users", users);
        modelAndView.addObject("requests", requests);
        modelAndView.addObject("group", group);
        return modelAndView;
    }

    @GetMapping("/{groupId}/updateRelation/{action}/{memberId}")
    public String updateFriendship(@PathVariable int memberId,
                                   @PathVariable int groupId,
                                   @PathVariable String action,
                                   @SessionAttribute("account") Account sessionAccount,
                                   @RequestHeader("referer") String referer) {
        logger.info("GET request to update relation between account id={}, group id={}.", memberId, groupId);
        if (memberId == sessionAccount.getId()) {
            updateSessionAccountRelation(sessionAccount, action, groupId);
        } else {
            updateMemberRelation(sessionAccount, action, groupId, memberId);
        }
        return "redirect:" + referer;
    }

    @RequestMapping(value = "/{groupId}/image", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getImage(@PathVariable int groupId) {
        logger.info("GET request to show image of group with id={}.", groupId);
        return groupService.getPhoto(groupId);
    }

    private void updateSessionAccountRelation(Account sessionAccount, String action, int groupId) {
        GroupRelation relation = groupService.getRelation(sessionAccount.getId(), groupId);
        switch (action) {
            case "sendRequest":
                groupService.sendRequest(sessionAccount.getId(), groupId);
                break;
            case "deleteRelation":
                groupService.deleteRelation(relation);
                break;
            default:
                break;
        }
    }

    private void updateMemberRelation(Account sessionAccount, String action, int groupId, int memberId) {
        if (!groupService.isAdmin(sessionAccount, groupService.get(groupId))) {
            throw new UIException("You cannot make changes (account with id=" + sessionAccount.getId() +
                    " is not admin of group with id=" + groupId + ")");
        }
        GroupRelation relation = groupService.getRelation(memberId, groupId);
        switch (action) {
            case "deleteRelation":
                groupService.deleteRelation(relation);
                break;
            case "makeUser":
                if (groupService.isAdmin(relation) && relation.getAccount().getId() == sessionAccount.getId()) {
                    logger.error("UIException in method updateMemberRelation: Administrator with id={} cannot make " +
                            "himself a user of group with id={}.", sessionAccount.getId(), groupId);
                    throw new UIException("You admin of this group (administrator with id=" + sessionAccount.getId() +
                            " cannot make himself a user of group with id=" + groupId + ").");
                }
                groupService.makeUser(relation);
                break;
            case "makeAdmin":
                groupService.makeAdmin(relation);
                break;
            default:
                break;
        }
    }

    @RequestMapping("/paginationSearch")
    @ResponseBody
    public List<Group> showAjaxSearchingResult(@RequestParam("search") String search,
                                               @RequestParam("page") int page,
                                               @RequestParam("maxResult") int maxResult) {
        logger.info("GET request to show pagination search groups with string={}.", search);
        return groupService.paginationSearch(search, page, maxResult);
    }
}