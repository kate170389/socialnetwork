package com.getjavajob.makhova.web;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.common.Group;
import com.getjavajob.makhova.common.Role;
import com.getjavajob.makhova.service.AccountService;
import com.getjavajob.makhova.service.GroupService;
import com.getjavajob.makhova.web.utils.RegistrationFormAccount;
import com.getjavajob.makhova.web.utils.UIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.makhova.web.utils.SecurityUtils.setAuthentication;

@Controller
public class ApplicationController {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
    private final AccountService accountService;
    private final GroupService groupService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ApplicationController(AccountService accountService, GroupService groupService, PasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping({"/", "/login"})
    public ModelAndView showLogin() {
        logger.info("GET request to show login page.");
        return new ModelAndView("login");
    }

    @GetMapping("/registration")
    public ModelAndView showRegistration() {
        logger.info("GET request to show registration page.");
        return new ModelAndView("registration");
    }

    @PostMapping("/registration")
    public ModelAndView doRegistration(@ModelAttribute RegistrationFormAccount registrationFormAccount,
                                       @RequestParam("file") MultipartFile file,
                                       HttpSession session) throws IOException {
        logger.info("POST request to registration account.");
        Account account = registrationFormAccount.getAccount();
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setRole(Role.USER);
        String email = account.getEmail();
        ModelAndView modelAndView = new ModelAndView("registration");
        if (accountService.get(email) != null) {
            modelAndView.addObject("error", "Error, email already exists");
            return modelAndView;
        }
        if (!file.isEmpty()) {
            byte[] photo = file.getBytes();
            account.setPhoto(photo);
        }
        accountService.createAccount(account);
        setAuthentication(account);
        session.setAttribute("account", account);
        return new ModelAndView("redirect:/account/" + account.getId());
    }

    @RequestMapping(value = "/image/{type}/{id}", method = RequestMethod.GET)
    public void showImage(
            @PathVariable int id,
            @PathVariable String type,
            @SessionAttribute("account") Account sessionAccount,
            HttpServletResponse resp) throws IOException {
        logger.info("GET request to show image.");
        byte[] content;
        switch (type) {
            case "group":
                content = groupService.getPhoto(id);
                break;
            case "account":
                if (sessionAccount.getId() == id) {
                    content = sessionAccount.getPhoto();
                } else {
                    content = accountService.getPhoto(id);
                }
                break;
            default:
                throw new UIException("Photo not found (invalid path variable type=" + type + ")");
        }
        resp.setContentType("image/jpeg");
        resp.setContentLength(content.length);
        resp.getOutputStream().write(content);
    }

    @GetMapping("/search")
    public ModelAndView showSearchingResult(@RequestParam("search") String searchingString) {
        logger.info("GET request to show accounts and groups with string={}.", searchingString);
        int accountsQty = accountService.getCountFromSearch(searchingString);
        int groupsQty = groupService.getCountFromSearch(searchingString);
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("accounts", accountsQty);
        modelAndView.addObject("groups", groupsQty);
        modelAndView.addObject("pattern", searchingString);
        return modelAndView;
    }

    @RequestMapping("/ajaxSearch")
    @ResponseBody
    public List<Object> showAjaxSearchingResult(@RequestParam("search") String search) {
        logger.info("GET request to show by ajax accounts and groups with string={}.", search);
        List<Object> res = new ArrayList<>();
        List<Account> accounts = accountService.getByString(search);
        List<Group> groups = groupService.getByString(search);
        res.addAll(accounts);
        res.addAll(groups);
        return res;
    }
}