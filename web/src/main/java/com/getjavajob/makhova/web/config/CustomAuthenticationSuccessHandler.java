package com.getjavajob.makhova.web.config;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.security.AccountDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.getjavajob.makhova.web.utils.SecurityUtils.getCurrentUser;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        HttpSession session = httpServletRequest.getSession();
        AccountDTO currentUser = getCurrentUser();
        Account account = currentUser == null ? null : currentUser.getAccount();
        System.out.println("ACCOUNT " + account);
        session.setAttribute("account", account);
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        if (account != null) {
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/account/" + account.getId());
        } else {
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/");
        }
    }
}