package com.getjavajob.makhova.web.utils;

public class UIException extends RuntimeException {
    private String messge;
    public UIException(String msg) {
        super(msg);
    }

    public UIException(Throwable cause) {
        super(cause);
    }

    public UIException() {
    }
}