package com.getjavajob.makhova.web;

import com.getjavajob.makhova.common.*;
import com.getjavajob.makhova.jms.JMSService;
import com.getjavajob.makhova.service.AccountService;
import com.getjavajob.makhova.web.utils.UIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/account")
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final JMSService jmsService;

    @Autowired
    public AccountController(AccountService accountService, PasswordEncoder passwordEncoder, JMSService jmsService) {
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
        this.jmsService = jmsService;
    }

    @GetMapping("/{accountId}")
    public ModelAndView showAccount(@PathVariable int accountId, @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to show account page with id={}.", accountId);
        int accountSessionId = sessionAccount.getId();
        Account account = accountService.get(accountId);
        String status = "noStatus";
        FriendRelation friendRelation = accountService.getFriendRelation(accountId, accountSessionId);
        int requests = 0;
        if (accountId == accountSessionId) {
            status = "admin";
            requests = accountService.getReceivedRequests(accountSessionId).size();
        }
        ModelAndView modelAndView = new ModelAndView("account/account");
        if (friendRelation == null) {
            modelAndView.addObject("relation", 0);
        } else {
            modelAndView.addObject("relation", friendRelation.getId());
            switch (friendRelation.getStatus()) {
                case ACCEPTED:
                    status = "friend";
                    break;
                case PENDING:
                    status = friendRelation.getActionAccount().getId() == accountSessionId ?
                            "sentRequest" : "receivedRequest";
                    break;
                case DECLINED:
                    status = friendRelation.getActionAccount().getId() == accountSessionId ?
                            "sentDecline" : "receivedDecline";
                    break;
                case BLOCKED:
                    status = friendRelation.getActionAccount().getId() == accountSessionId ?
                            "sentBlock" : "receivedBlock";
                    break;
                default:
                    status = "noStatus";
                    break;
            }
        }
        modelAndView.addObject("account", account);
        modelAndView.addObject("status", status);
        modelAndView.addObject("requests", requests);
        modelAndView.addObject("messages", accountService.getMessagesToAccount(MessageType.COMMON, account));
        return modelAndView;
    }

    @RequestMapping(value = "/{accountId}/image", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getImage(@PathVariable int accountId, @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to show account image with id={}.", accountId);
        byte[] content;
        if (sessionAccount.getId() == accountId) {
            content = sessionAccount.getPhoto();
        } else {
            content = accountService.getPhoto(accountId);
        }
        return content;
    }

    @GetMapping("/{accountId}/update/{type}")
    public ModelAndView showUpdateAccount(@PathVariable int accountId,
                                          @PathVariable String type,
                                          @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to update account with id={}.", accountId);
        ModelAndView modelAndView;
        Account account = accountService.get(accountId);
        switch (type) {
            case "photo":
                if (accountId != sessionAccount.getId()) {
                    throw new UIException("You cannot make changes(account with id=" + sessionAccount.getId() +
                            " can't make changes in account with id=" + accountId + ")");
                } else {
                    modelAndView = new ModelAndView("account/updateAccountPhoto");
                    modelAndView.addObject("account", account);
                }
                break;
            case "info":
                if (accountId != sessionAccount.getId()) {
                    throw new UIException("You cannot make changes(account with id=" + sessionAccount.getId() +
                            " can't make changes in account with id=" + accountId + ")");
                } else {
                    List<Phone> workPhones = new ArrayList<>();
                    List<Phone> personalPhones = new ArrayList<>();
                    distributePhones(workPhones, personalPhones, account);
                    modelAndView = new ModelAndView("account/updateAccount");
                    modelAndView.addObject("account", accountService.get(accountId));
                    modelAndView.addObject("workPhones", workPhones);
                    modelAndView.addObject("personalPhones", personalPhones);
                }
                break;
            case "delete":
                if (accountId == sessionAccount.getId()) {
                    modelAndView = new ModelAndView("redirect:/logout");
                } else {
                    modelAndView = new ModelAndView("deletedPage");
                    modelAndView.addObject("message", "Account " + account.getName() + " has been deleted.");
                }
                accountService.delete(account);
                break;
            default:
                modelAndView = new ModelAndView("redirect:/account/" + accountId);
                break;
        }
        return modelAndView;
//        }
    }

    @PostMapping("/{accountId}/update/photo")
    public ModelAndView doUpdateAccount(@PathVariable int accountId,
                                        @RequestParam("file") MultipartFile file,
                                        @SessionAttribute("account") Account sessionAccount,
                                        HttpSession session) throws IOException {
        logger.info("POST request to update account photo with id={}.", accountId);
        if (accountId != sessionAccount.getId()) {
            throw new UIException("You cannot make changes (account with id=" + sessionAccount.getId() +
                    " can't make changes in account with id=" + accountId + ")");
        } else {
            Account account = accountService.get(accountId);
            if (!file.isEmpty()) {
                byte[] photo = file.getBytes();
                accountService.uploadPhoto(photo, account);
            }
            if (accountId == sessionAccount.getId()) {
                session.setAttribute("account", account);
            }
            return new ModelAndView("redirect:/account/" + accountId);
        }
    }

    @PostMapping("/{accountId}/update/info")
    public ModelAndView doUpdateInfo(@ModelAttribute Account account,
                                     @RequestParam("accountBirthDate") String birthDate,
                                     @SessionAttribute("account") Account sessionAccount,
                                     HttpSession session,
                                     @CookieValue(value = "remember-me", required = false) Cookie rememberMeCookie,
                                     HttpServletResponse resp,
                                     HttpServletRequest req,
                                     @PathVariable("accountId") String accountId) throws ServletException {
        logger.info("POST request to update account info with id={}.", accountId);
        account.setBirthDate(LocalDate.parse(birthDate));
        Account checkAccount = accountService.get(account.getEmail());
        ModelAndView modelAndView;
        if (checkAccount != null && checkAccount.getId() != account.getId()) {
            logger.info("Email {} already exist.", account.getEmail());
            List<Phone> workPhones = new ArrayList<>();
            List<Phone> personalPhones = new ArrayList<>();
            distributePhones(workPhones, personalPhones, account);
            modelAndView = new ModelAndView("account/updateAccount");
            modelAndView.addObject("account", account);
            modelAndView.addObject("workPhones", workPhones);
            modelAndView.addObject("personalPhones", personalPhones);
            modelAndView.addObject("error", "Error, email already exists");
        } else {
            Account oldAccount = accountService.get(account.getId());
            account.setRole(oldAccount.getRole());
            account.setPhoto(oldAccount.getPhoto());
            account.setPassword(account.getPassword().isEmpty() ? oldAccount.getPassword() :
                    passwordEncoder.encode(account.getPassword()));
            accountService.update(account);
            if (account.getId() == sessionAccount.getId()) {
                session.setAttribute("account", account);
            }
            modelAndView = new ModelAndView("redirect:/account/" + account.getId());
        }
        return modelAndView;
    }

    @GetMapping("/updateRelationship/{action}/{friendId}")
    public String updateFriendship(@PathVariable int friendId,
                                   @PathVariable String action,
                                   @SessionAttribute("account") Account sessionAccount,
                                   @RequestHeader("referer") String referer) {
        logger.info("GET request to update friend relation between accounts with id={} and id={}.",
                sessionAccount.getId(), friendId);
        int accountId = sessionAccount.getId();
        switch (action) {
            case "sendRequest":
                accountService.sendFriendRequest(accountId, friendId);
                Account accountTo = accountService.get(friendId);
                String date = LocalDate.now().toString();
                JMSFriendRequest jmsAccount=new JMSFriendRequest(sessionAccount.getEmail(), accountTo.getEmail(), date);
                jmsService.sendMessage(jmsAccount);
                break;
            case "delete":
                accountService.deleteFriendRelation(accountId, friendId);
                break;
            case "accept":
                accountService.acceptFriendRequest(accountId, friendId);
                break;
            case "decline":
                accountService.declineFriendRequest(accountId, friendId);
                break;
            case "block":
                accountService.blockUser(accountId, friendId);
                break;
            default:
                break;
        }
        return "redirect:" + referer;
    }

    @GetMapping("/{accountId}/groups")
    public ModelAndView showGroups(@PathVariable int accountId,
                                   @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to show groups for account with id={}.", accountId);
        List<Group> groups;
        String relation;
        List<Group> adminGroups = accountService.getAdminGroups(accountId);
        if (sessionAccount.getId() == accountId) {
            relation = "admin";
            groups = accountService.getUserGroups(accountId);
        } else {
            groups = accountService.getGroups(accountId);
            relation = "notAdmin";
        }
        ModelAndView modelAndView = new ModelAndView("account/groups");
        modelAndView.addObject("relation", relation);
        modelAndView.addObject("adminGroups", adminGroups);
        modelAndView.addObject("groups", groups);
        return modelAndView;
    }

    @GetMapping("/{accountId}/friends")
    public ModelAndView showFriends(@PathVariable int accountId,
                                    @SessionAttribute("account") Account sessionAccount) {
        logger.info("GET request to show friends for account with id={}.", accountId);
        int sessionAccountId = sessionAccount.getId();
        FriendRelation relation = accountService.getFriendRelation(accountId, sessionAccountId);
        if (relation != null && relation.getStatus() == FriendStatus.BLOCKED &&
                relation.getActionAccount().getId() == accountId) {
            throw new UIException("You are blocked (Account with id=" + sessionAccount.getId() +
                    " does not access to account with id=" + accountId + "})");
        }
        List<Account> friends;
        List<Account> requests = new ArrayList<>();
        List<Account> blocked = new ArrayList<>();
        String status;
        ModelAndView modelAndView = new ModelAndView("account/friends");
        if (sessionAccountId == accountId) {
            status = "admin";
            modelAndView.addObject("account", sessionAccount);
            friends = accountService.getFriends(sessionAccountId);
            friends.addAll(accountService.getSentRequests(sessionAccountId));
            friends.addAll(accountService.getDeclinedRequests(sessionAccountId));
            requests = accountService.getReceivedRequests(sessionAccountId);
            blocked = accountService.getBlocked(sessionAccountId);
        } else {
            status = "notAdmin";
            Account account = accountService.get(accountId);
            modelAndView.addObject("account", account);
            friends = accountService.getFriends(accountId);
        }
        modelAndView.addObject("status", status);
        modelAndView.addObject("friends", friends);
        modelAndView.addObject("requests", requests);
        modelAndView.addObject("blocked", blocked);
        return modelAndView;
    }

    private void distributePhones(List<Phone> workPhones, List<Phone> personalPhones, Account account) {
        for (Phone phone : account.getPhones()) {
            if (phone.getType().name().equals("WORK")) {
                workPhones.add(phone);
            } else {
                personalPhones.add(phone);
            }
        }
    }

    @GetMapping("/paginationSearch")
    @ResponseBody
    public List<Account> showAjaxSearchingResult(@RequestParam("search") String search,
                                                 @RequestParam("page") int page,
                                                 @RequestParam("maxResult") int maxResult) {
        logger.info("GET request to show pagination search accounts with string={}.", search);
        return accountService.paginationSearch(search, page, maxResult);
    }

    @GetMapping("/xmlConverter")
    public ModelAndView showXmlPage() {
        logger.info("GET request to show xml converter page.");
        return new ModelAndView("account/xmlConverter");
    }

    @PostMapping("/xmlConverter/fromXml")
    public ModelAndView accountFromXml(@RequestParam("fileXml") MultipartFile fileXml) throws JAXBException,
            IOException {
        logger.info("POST request get account from file.");
        ModelAndView modelAndView = new ModelAndView("account/updateAccount");
        JAXBContext jaxbContext = JAXBContext.newInstance(Account.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        InputStream bais = new ByteArrayInputStream(fileXml.getBytes());
        Account account = (Account) jaxbUnmarshaller.unmarshal(bais);
        List<Phone> workPhones = new ArrayList<>();
        List<Phone> personalPhones = new ArrayList<>();
        distributePhones(workPhones, personalPhones, account);
        modelAndView.addObject("account", account);
        modelAndView.addObject("workPhones", workPhones);
        modelAndView.addObject("personalPhones", personalPhones);
        return modelAndView;
    }

    @GetMapping("/xmlConverter/toXml")
    public void saveToXml(@SessionAttribute("account") Account account,
                          HttpServletResponse response) throws JAXBException, IOException {
        logger.info("GET request save account with id={} to xml file.", account.getId());
        response.setContentType("text/xml");
        response.setHeader("Content-disposition", "attachment; filename=account.xml");
        JAXBContext context1 = JAXBContext.newInstance(Account.class);
        Marshaller marshaller = context1.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        File file = new File("account.xml");
        marshaller.marshal(account, file);
        OutputStream outputStream = response.getOutputStream();
        FileInputStream inputStream = new FileInputStream(file);
        BufferedInputStream buf = new BufferedInputStream(inputStream);
        int readBytes;
        while ((readBytes = buf.read()) != -1) {
            outputStream.write(readBytes);
        }
        buf.close();
        inputStream.close();
        outputStream.flush();
    }

/*    using the Spring download service
    @RequestMapping(value = "/xmlConverter/toXml",  produces = "text/xml")
    @ResponseBody
    public Account saveToXml(@SessionAttribute("account") Account account,
                             HttpServletResponse response) {
        response.setContentType("text/xml");
        response.setHeader("Content-disposition", "attachment; filename=account.xml");
        return account;
    }*/
}