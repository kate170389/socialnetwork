package com.getjavajob.makhova.web.utils;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.common.Gender;
import com.getjavajob.makhova.common.Phone;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RegistrationFormAccount {
    private String name;
    private String surName;
    private String middleName;
    private String email;
    private String password;
    private Gender gender;
    private String birthDate;

    public void setName(String name) {
        this.name = name;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Account getAccount() {
        Account account = new Account();
        account.setName(name);
        account.setSurName(surName);
        account.setMiddleName(middleName.isEmpty() ? null : middleName);
        account.setEmail(email);
        account.setPassword(password);
        account.setGender(gender);
        account.setBirthDate(LocalDate.parse(birthDate));
        List<Phone> phones = new ArrayList<>();
        account.setPhones(phones);
        return account;
    }
}
