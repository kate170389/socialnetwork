package com.getjavajob.makhova.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ErrorController {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex, HttpServletResponse resp) {
        logger.error("Error:", ex);
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("status", resp.getStatus());
        modelAndView.addObject("url", req.getRequestURL());
        return modelAndView;
    }
}