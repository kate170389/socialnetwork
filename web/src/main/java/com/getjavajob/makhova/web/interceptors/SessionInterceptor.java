package com.getjavajob.makhova.web.interceptors;

import com.getjavajob.makhova.common.Account;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.getjavajob.makhova.web.utils.SecurityUtils.getCurrentUser;

@Component
public class SessionInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object handler)
            throws Exception {
        HttpSession session = req.getSession(true);
        if (getCurrentUser() != null) {
            resp.sendRedirect(req.getContextPath() + "/account/" +
                    ((Account) session.getAttribute("account")).getId());
            return false;
        }
        return super.preHandle(req, resp, handler);
    }
}