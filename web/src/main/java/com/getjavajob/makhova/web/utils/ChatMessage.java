package com.getjavajob.makhova.web.utils;

public class ChatMessage {
    private String accountFromEmail;
    private String accountToEmail;
    private String text;
    private String time;
    private int accountFromId;
    private String accountFromName;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAccountFromEmail() {
        return accountFromEmail;
    }

    public void setAccountFromEmail(String accountFromEmail) {
        this.accountFromEmail = accountFromEmail;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAccountToEmail() {
        return accountToEmail;
    }

    public void setAccountToEmail(String accountToEmail) {
        this.accountToEmail = accountToEmail;
    }

    public int getAccountFromId() {
        return accountFromId;
    }

    public void setAccountFromId(int accountFromId) {
        this.accountFromId = accountFromId;
    }

    public String getAccountFromName() {
        return accountFromName;
    }

    public void setAccountFromName(String accountFromName) {
        this.accountFromName = accountFromName;
    }
}