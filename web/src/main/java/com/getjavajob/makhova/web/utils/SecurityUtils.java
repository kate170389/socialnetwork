package com.getjavajob.makhova.web.utils;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.security.AccountDTO;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityUtils {
    private SecurityUtils() {
    }

    public static AccountDTO getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth instanceof AnonymousAuthenticationToken ? null : (AccountDTO) auth.getPrincipal();
    }

    public static void setAuthentication(Account account) {
        UserDetails accountForSecurity = new AccountDTO(account);
        Authentication auth = new UsernamePasswordAuthenticationToken(accountForSecurity, null,
                accountForSecurity.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}