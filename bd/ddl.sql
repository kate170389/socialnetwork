CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surName` varchar(45) NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `gender` char(6) DEFAULT NULL,
  `birthDay` date NOT NULL,
  `homeAddress` varchar(45) DEFAULT NULL,
  `workAddress` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `icq` char(9) DEFAULT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `additionalInfo` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `logo` blob,
  `role` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
)

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `dateOfCreation` date NOT NULL,
  `logo` blob,
  PRIMARY KEY (`id`)
)

CREATE TABLE `account_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Account_id` int(11) NOT NULL,
  `Group_id` int(11) NOT NULL,
  `status` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `Group_account_UNIQUE` (`Account_id`,`Group_id`),
  KEY `Account_idFK_idx` (`Account_id`),
  KEY `Group_idFK_idx` (`Group_id`),
  CONSTRAINT `Account_idFK` FOREIGN KEY (`Account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Group_idFK` FOREIGN KEY (`Group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_from` int(11) NOT NULL,
  `account_to` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `photo` blob,
  `type` varchar(8) NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `account_from_idx` (`account_from`),
  KEY `account_to_idx` (`account_to`),
  CONSTRAINT `account_from` FOREIGN KEY (`account_from`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `account_to` FOREIGN KEY (`account_to`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)

CREATE TABLE `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) NOT NULL,
  `type` varchar(8) NOT NULL,
  `number` varchar(17) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Account_id_FK_idx` (`id_account`),
  CONSTRAINT `Account_id_FK` FOREIGN KEY (`id_account`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)

CREATE TABLE `relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one_id` int(11) NOT NULL,
  `user_two_id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `action_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_one_two_UNIQUE` (`user_one_id`,`user_two_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_two_FK_idx` (`user_two_id`),
  KEY `user_one_FK_idx` (`user_one_id`),
  KEY `action_id_FK_idx` (`action_id`),
  CONSTRAINT `user_one_FK` FOREIGN KEY (`user_one_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_two_FK` FOREIGN KEY (`user_two_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `action_id_FK` FOREIGN KEY (`action_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
)