###Social Network
   
**Functionality:**
   
   + registration  
   + authentication with Spring Security
   + display profile  
   + edit profile     
   + upload avatar  
   + ajax search with pagination  
   + ajax search with autocomplete  
   + users export to xml  
   + user import from xml
   + add and remove friends
   + edit friendship status
   + add/remove/edit groups
   + edit group member status
   + user chat with WebSockets
   + sending messages to the server for further mailing to users
   
   **Tools:**  
   JDK 7,8, Spring 5, JPA 2/Hibernate 5, jQuery, Twitter Bootstrap 4, JavaScript, JUnit 4, JSP/JSTL, Mockito, Maven 3, Log4j, Spring-boot 2, Git/Bitbucket, Tomcat 9, MySQL, IntelliJIDEA, JMS/ActiveMQ.
    
   **Notes:**  
   SQL ddl is located in the `db/ddl.sql`
   
   **Screenshots:**
   ![Login page](screenshots/login.png)
   ![Account page](screenshots/account_page.png)
   ![Update page](screenshots/edit_account.png)
   ![Autocomplete](screenshots/ajax_earch_autocomplete.png)
   ![Pagination serch](screenshots/pagination_search2.png)
   ![Chat](screenshots/chat.png)
   
   project hosted: https://heroku-socnet.herokuapp.com/  
   login: admin@mail.ru  
   password: admin
   
   --  
**Makhova Ekaterina**  
Training getJavaJob  
http://www.getjavajob.com