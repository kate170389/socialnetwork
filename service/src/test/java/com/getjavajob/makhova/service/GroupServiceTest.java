package com.getjavajob.makhova.service;

import com.getjavajob.makhova.common.*;
import com.getjavajob.makhova.dao.GroupDao;
import com.getjavajob.makhova.dao.GroupRelationDAO;
import com.getjavajob.makhova.dao.RelationshipDAO;
import com.getjavajob.makhova.dao.springdatajpa.AccountRepository;
import com.getjavajob.makhova.utils.ValidationDAOException;
import com.getjavajob.makhova.utils.ValidationServiceException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    @Mock
    private AccountRepository accountDAO;
    @Mock
    private GroupDao groupDao;
    //    @Mock
//    private MessageRepository messageDAO;
    @Mock
    private RelationshipDAO relationshipDAO;
    @Mock
    private GroupRelationDAO groupRelationDAO;

    @InjectMocks
    private GroupService groupService;
    private Account account;
    private Group group;
    private GroupRelation relation1;

    @Before
    public void init() {
        account = new Account();
        account.setName("Ivan");
        account.setSurName("Ivanov");
        account.setMiddleName("Ivanovich");
        account.setGender(Gender.MALE);
        account.setBirthDate(LocalDate.of(1996, 2, 10));
        account.setEmail("ivan@mail.ru");
        group = new Group("group1", "one", LocalDate.of(2019, 10, 11));
        relation1 = new GroupRelation(account, group, GroupStatus.ADMIN);
        System.out.println("Before");
    }

    @After
    public void destroy() {
        System.out.println("After");
    }

    @Test
    public void createGroup() throws ValidationDAOException {
        when(groupDao.insert(group)).thenReturn(group);
        assertEquals(group, groupService.createGroup(account, group));
        verify(groupDao, times(1)).insert(group);
    }

    @Test(expected = ValidationServiceException.class)
    public void createGroupException() throws ValidationDAOException {
        doThrow(new ValidationDAOException()).when(groupDao).insert(anyObject());
        groupService.createGroup(account, group);
    }

    @Test
    public void updateGroup() throws ValidationDAOException {
        when(groupDao.update(group)).thenReturn(group);
        when(groupRelationDAO.getByTwoId(account.getId(), group.getId())).thenReturn(relation1);
        assertEquals(group, groupService.update(group, account));
        verify(groupDao, times(1)).update(group);
    }

    @Test(expected = ValidationServiceException.class)
    public void updateGroupNotAdminException() throws ValidationDAOException {
        when(groupDao.update(group)).thenReturn(group);
        relation1.setStatus(GroupStatus.USER);
        when(groupRelationDAO.getByTwoId(account.getId(), group.getId())).thenReturn(relation1);
        groupService.update(group, account);
    }

    @Test(expected = ValidationServiceException.class)
    public void updateGroupNotFoundException() throws ValidationDAOException {
        doThrow(new ValidationDAOException()).when(groupDao).update(anyObject());
        when(groupRelationDAO.getByTwoId(account.getId(), group.getId())).thenReturn(relation1);
        groupService.update(group, account);
    }

    @Test
    public void deleteGroup() throws ValidationDAOException {
        when(groupDao.delete(0)).thenReturn(1);
        when(groupRelationDAO.getByTwoId(account.getId(), group.getId())).thenReturn(relation1);
        assertEquals(1, groupService.delete(group, account));
        verify(groupDao, times(1)).delete(group.getId());
    }

    @Test(expected = ValidationServiceException.class)
    public void deleteGroupNotAdminException() throws ValidationDAOException {
        when(groupDao.delete(group.getId())).thenReturn(1);
        relation1.setStatus(GroupStatus.USER);
        when(groupRelationDAO.getByTwoId(account.getId(), group.getId())).thenReturn(relation1);
        groupService.delete(group, account);
    }

    @Test(expected = ValidationServiceException.class)
    public void deleteGroupNotFoundException() throws ValidationDAOException {
        doThrow(new ValidationDAOException()).when(groupDao).delete(anyInt());
        when(groupRelationDAO.getByTwoId(account.getId(), group.getId())).thenReturn(relation1);
        groupService.delete(group, account);
    }

    @Test
    public void get() {
        when(groupDao.get(0)).thenReturn(group);
        assertEquals(group, groupService.get(group.getId()));
        verify(groupDao, times(1)).get(anyInt());
    }

    @Test(expected = ValidationServiceException.class)
    public void getNotFoundGroup() throws ValidationDAOException {
        when(groupDao.get(0)).thenReturn(null);
        groupService.get(group.getId());
    }

    @Test
    public void getByString() {
        List<Group> groups = new ArrayList<>();
        groups.add(group);
        when(groupDao.getElementsByString("Iv")).thenReturn(groups);
        assertEquals(groups, groupService.getByString("Iv"));
        verify(groupDao, times(1)).getElementsByString(anyString());
    }

    @Test
    public void sendRequest() throws ValidationDAOException {
        when(accountDAO.findById(0)).thenReturn(Optional.of(account));
        when(groupDao.get(0)).thenReturn(group);
        relation1.setStatus(GroupStatus.REQUEST);
        when(groupRelationDAO.insert(relation1)).thenReturn(relation1);
        assertEquals(0, groupService.sendRequest(account.getId(), group.getId()));
        verify(groupRelationDAO, times(1)).insert(any());
    }
}