package com.getjavajob.makhova.service;

import com.getjavajob.makhova.common.*;
import com.getjavajob.makhova.dao.GroupDao;
import com.getjavajob.makhova.dao.GroupRelationDAO;
import com.getjavajob.makhova.dao.RelationshipDAO;
import com.getjavajob.makhova.dao.springdatajpa.AccountRepository;
import com.getjavajob.makhova.dao.springdatajpa.MessageRepository;
import com.getjavajob.makhova.utils.ValidationServiceException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @Mock
    private AccountRepository accountDAO;
    @Mock
    private GroupDao groupDao;
    @Mock
    private MessageRepository messageDAO;
    @Mock
    private RelationshipDAO relationshipDAO;
    @Mock
    private GroupRelationDAO groupRelationDAO;

    @InjectMocks
    private AccountService accountService;
    private Phone phone;
    private Phone phone2;
    private Account account;
    private Group groupOne;
    private Group groupTwo;
    private Group groupThree;
    private GroupRelation relation1;
    private GroupRelation relation2;
    private GroupRelation relation3;

    @Before
    public void init() {
        phone = new Phone();
        phone.setNumber("+7(495)-162-16-28");
        phone.setType(PhoneType.PERSONAL);
        phone2 = new Phone();
        phone2.setNumber("+7(495)-166-69-99");
        phone2.setType(PhoneType.WORK);
        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        phones.add(phone2);
        account = new Account();
        account.setName("Ivan");
        account.setSurName("Ivanov");
        account.setMiddleName("Ivanovich");
        account.setGender(Gender.MALE);
        account.setBirthDate(LocalDate.of(1996, 2, 10));
        account.setEmail("ivan@mail.ru");
        account.setPhones(phones);
        groupOne = new Group("group1", "one", LocalDate.of(2019, 10, 11));
        groupOne.setId(1);
        groupTwo = new Group("group2", "two", LocalDate.of(2019, 10, 11));
        groupTwo.setId(2);
        groupThree = new Group("group3", "three", LocalDate.of(2019, 10, 11));
        groupThree.setId(3);
        relation1 = new GroupRelation(account, groupOne, GroupStatus.ADMIN);
        relation2 = new GroupRelation(account, groupTwo, GroupStatus.REQUEST);
        relation3 = new GroupRelation(account, groupThree, GroupStatus.USER);
        System.out.println("Before");
    }

    @After
    public void destroy() {
        System.out.println("After");
    }

    @Test
    public void createAccount() {
        int count = 1;
        Account newAccount = account.clone();
        newAccount.setId(count);
        for (Phone phone : newAccount.getPhones()) {
            phone.setId(count++);
        }
        when(accountDAO.save(account)).thenReturn(newAccount);
        Account actual = accountService.createAccount(account);
        assertEquals(newAccount, actual);
        verify(this.accountDAO, times(1)).save(account);
    }

    @Test(expected = ValidationServiceException.class)
    public void createAccountException() {
        account.setId(1);
        accountService.createAccount(account);
    }


    @Test
    public void update() {
        account.setId(1);
        phone.setNumber(null);
        phone.setId(1);
        phone2.setId(2);
        when(accountDAO.existsById(1)).thenReturn(true);
        when(accountDAO.save(account)).thenReturn(account);
        Account expected = account.clone();
        for (Phone phone : expected.getPhones()) {
            if (phone.getNumber() == null) {
                phone = null;
            } else {
                phone.setAccount(expected);
            }
        }
        Account actual = accountService.update(account);
        assertEquals(expected, actual);
        verify(accountDAO, times(1)).save(account);
    }

    @Test(expected = ValidationServiceException.class)
    public void updateAccountException() {
        System.out.println(account.getId());
        when(accountDAO.existsById(0)).thenReturn(false);
        accountService.update(account);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteException() {
        doThrow(new EmptyResultDataAccessException(0)).when(accountDAO).deleteById(anyObject());
        when(relationshipDAO.deleteRelations(0)).thenReturn(0);
        accountService.delete(account);
    }

    @Test
    public void getElementsByString() {
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        when(accountDAO.findAllByString("va", Sort.by("name"))).thenReturn(accounts);
        when(accountDAO.findAllByString("нет", Sort.by("name"))).thenReturn(new ArrayList<Account>());
        List<Account> expected = new ArrayList<>();
        expected.add(account);
        assertEquals(expected, accountService.getByString("va"));
        expected = new ArrayList<>();
        assertEquals(expected, accountService.getByString("нет"));
    }

    @Test
    public void get() {
        Optional<Account> optional = Optional.of(account);
        when(accountDAO.findById(0)).thenReturn(optional);
        when(accountDAO.findByEmail("ivan@mail.ru")).thenReturn(account);
        assertEquals(account, accountService.get(account.getId()));
        assertEquals(account, accountService.get(account.getEmail()));
        verify(accountDAO, times(1)).findById(anyInt());
        verify(accountDAO, times(1)).findByEmail(anyString());
    }

    @Test
    public void getAdminGroups() {
        List<GroupRelation> relations = new ArrayList<>();
        relations.add(relation1);
        relations.add(relation2);
        relations.add(relation3);
        when(groupRelationDAO.getByAccountId(0)).thenReturn(relations);
        when(groupDao.get(1)).thenReturn(groupOne);
        when(groupDao.get(2)).thenReturn(groupTwo);
        when(groupDao.get(3)).thenReturn(groupThree);
        List<Group> expected = new ArrayList<>();
        expected.add(groupOne);
        assertEquals(expected, accountService.getAdminGroups(account.getId()));
    }

    @Test
    public void getGroups() {
        List<GroupRelation> relations = new ArrayList<>();
        relations.add(relation1);
        relations.add(relation2);
        relations.add(relation3);
        when(groupRelationDAO.getByAccountId(0)).thenReturn(relations);
        when(groupDao.get(1)).thenReturn(groupOne);
        when(groupDao.get(2)).thenReturn(groupTwo);
        when(groupDao.get(3)).thenReturn(groupThree);
        List<Group> expected = new ArrayList<>();
        expected.add(groupOne);
        expected.add(groupTwo);
        expected.add(groupThree);
        assertEquals(expected, accountService.getGroups(account.getId()));
    }
}