package com.getjavajob.makhova.utils;

public class ValidationServiceException extends RuntimeException {
    public ValidationServiceException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ValidationServiceException(String message) {
        super(message);
    }

    public ValidationServiceException(Throwable cause) {
        super(cause);
    }
}