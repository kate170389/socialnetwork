package com.getjavajob.makhova.utils;

public class DuplicateServiceException extends RuntimeException {
    public DuplicateServiceException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DuplicateServiceException( Throwable cause) {
        super(cause);
    }
}
