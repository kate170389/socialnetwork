package com.getjavajob.makhova.service;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.common.Group;
import com.getjavajob.makhova.common.GroupRelation;
import com.getjavajob.makhova.common.GroupStatus;
import com.getjavajob.makhova.dao.GroupDao;
import com.getjavajob.makhova.dao.GroupRelationDAO;
import com.getjavajob.makhova.dao.springdatajpa.AccountRepository;
import com.getjavajob.makhova.utils.ValidationDAOException;
import com.getjavajob.makhova.utils.ValidationServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GroupService {
    private static final Logger logger = LoggerFactory.getLogger(GroupService.class);
    private GroupDao groupDao;
    private AccountRepository accountDao;
    private GroupRelationDAO groupRelationDao;

    @Autowired
    public GroupService(GroupDao groupDao, AccountRepository accountDAO, GroupRelationDAO groupRelationDAO) {
        this.groupDao = groupDao;
        this.accountDao = accountDAO;
        this.groupRelationDao = groupRelationDAO;
    }

    @Transactional
    public Group createGroup(Account admin, Group group) {
        logger.debug("Create new group with name={} and admin id={}.", group.getName(), admin.getId());
        try {
            Group newGroup = groupDao.insert(group);
            GroupRelation groupRelation = new GroupRelation(admin, group, GroupStatus.ADMIN);
            groupRelationDao.insert(groupRelation);
            return newGroup;
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException(e);
        }
    }

    @Transactional
    public Group update(Group group, Account admin) {
        logger.debug("Update group with name={}, admin id={}.", group.getName(), admin.getId());
        if (!isAdmin(admin, group)) {
            throw new ValidationServiceException("Account with id=" + admin.getId() +
                    " can't make changes in group with id=" + group.getId() + ".");
        }
        try {
            return groupDao.update(group);
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException("Group with id=" + group.getId() + " not found", e);
        }
    }

    @Transactional
    public int delete(Group group, Account admin) {
        logger.debug("Delete group with id={}.", group.getId());
        if (!isAdmin(admin, group)) {
            throw new ValidationServiceException("Account with id=" + admin.getId() +
                    " can't make changes in group with id=" + group.getId() + ".");
        }
        try {
            return groupDao.delete(group.getId());
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException("Group with id=" + group.getId() + " not found.", e);
        }
    }

    public Group get(int id) {
        logger.debug("Get group with id={}.", id);
        Group group = groupDao.get(id);
        if (group != null) {
            return group;
        } else {
            throw new ValidationServiceException("Group with id=" + id + " not found.");
        }
    }

    public List<Group> getByString(String string) {
        logger.debug("Get groups for string={}.", string);
        return groupDao.getElementsByString(string);
    }

    public List<Group> getAll() {
        logger.debug("Get all groups.");
        return groupDao.getAll();
    }

    @Transactional
    public int sendRequest(int accountId, int groupId) {
        logger.debug("Send request from account with id={} to group with id={}.", accountId, groupId);
        try {
            Optional<Account> optional = accountDao.findById(accountId);
            if (!optional.isPresent()) {
                throw new ValidationServiceException("Account with id=" + accountId + " not found.");
            }
            GroupRelation groupRelation = new GroupRelation(optional.get(), groupDao.get(groupId),
                    GroupStatus.REQUEST);
            return groupRelationDao.insert(groupRelation).getId();
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException(e);
        }
    }

    @Transactional
    public int deleteRelation(GroupRelation relation) {
        logger.debug("Delete group relationwith id={}.", relation.getId());
        if (isAdmin(relation)) {
            throw new ValidationServiceException("Admin can't leave the group, relation with id=" +
                    relation.getId() + " can't be deleted.");
        }
        try {
            return groupRelationDao.delete(relation.getId());
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException(e);
        }
    }

    public boolean isAdmin(Account account, Group group) {
        logger.debug("Check account with id={} is admin of group with id={}.", account.getId(), group.getId());
        GroupRelation relation = groupRelationDao.getByTwoId(account.getId(), group.getId());
        return relation != null && relation.getStatus() == GroupStatus.ADMIN;
    }

    public boolean isAdmin(GroupRelation relation) {
        return relation != null && relation.getStatus() == GroupStatus.ADMIN;
    }

    public GroupRelation getRelation(int accountId, int groupId) {
        logger.debug("Get relation for account with id={} and group with id={}.", accountId, groupId);
        return groupRelationDao.getByTwoId(accountId, groupId);
    }

    public List<Account> getAdmins(int groupId) {
        logger.debug("Get admins for group with id={}.", groupId);
        List<GroupRelation> relations = groupRelationDao.getByGroupId(groupId);
        return getRelatedAccounts(relations, GroupStatus.ADMIN);
    }

    public List<Account> getUsers(int groupId) {
        logger.debug("Get users for group with id={}.", groupId);
        List<GroupRelation> relations = groupRelationDao.getByGroupId(groupId);
        return getRelatedAccounts(relations, GroupStatus.USER);
    }

    public List<Account> getRequests(int groupId) {
        logger.debug("Get requests for group with id={}.", groupId);
        List<GroupRelation> relations = groupRelationDao.getByGroupId(groupId);
        return getRelatedAccounts(relations, GroupStatus.REQUEST);
    }

    @Transactional
    public GroupRelation makeAdmin(GroupRelation relation) {
        logger.debug("Make admin status for relation with id={}.", relation.getId());
        relation.setStatus(GroupStatus.ADMIN);
        try {
            return groupRelationDao.update(relation);
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException("Group for update not found.");
        }
    }

    @Transactional
    public GroupRelation makeUser(GroupRelation relation) {
        logger.debug("Make user status for relation with id={}.", relation.getId());
        relation.setStatus(GroupStatus.USER);
        try {
            return groupRelationDao.update(relation);
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException("Group for update not found.");
        }
    }

    public byte[] getPhoto(int id) {
        logger.debug("Get photo to group with id={}.", id);
        return groupDao.getPhoto(id);
    }

    private List<Account> getRelatedAccounts(List<GroupRelation> relations, GroupStatus status) {
        List<Account> accounts = new ArrayList<>();
        relations.forEach(relation -> {
            if (relation.getStatus() == status) {
                Optional<Account> optional = accountDao.findById(relation.getAccount().getId());
                if (!optional.isPresent()) {
                    throw new ValidationServiceException("Account with id=" + relation.getAccount().getId() + " not found.");
                }
                accounts.add(optional.get());
            }
        });
        return accounts;
    }

    public List<Group> paginationSearch(String string, int page, int pageSize) {
        logger.debug("Pagination search gorup with string={}, in page={}, number of entries per page={}.",
                string, page, pageSize);
        return groupDao.getPaginationSearch(string, page, pageSize);
    }

    public int getCountFromSearch(String string) {
        return getByString(string).size();
    }
}