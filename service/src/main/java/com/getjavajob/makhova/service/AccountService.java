package com.getjavajob.makhova.service;

import com.getjavajob.makhova.common.*;
import com.getjavajob.makhova.dao.GroupDao;
import com.getjavajob.makhova.dao.GroupRelationDAO;
import com.getjavajob.makhova.dao.RelationshipDAO;
import com.getjavajob.makhova.dao.springdatajpa.AccountRepository;
import com.getjavajob.makhova.dao.springdatajpa.MessageRepository;
import com.getjavajob.makhova.security.AccountDTO;
import com.getjavajob.makhova.utils.ValidationDAOException;
import com.getjavajob.makhova.utils.ValidationServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    private AccountRepository accountDAO;
    private MessageRepository messageDAO;
    private GroupDao groupDao;
    private GroupRelationDAO groupRelationDAO;
    private RelationshipDAO relationshipDAO;

    @Autowired
    public AccountService(AccountRepository accountDAO, MessageRepository messageDAO, RelationshipDAO relationshipDAO,
                          GroupRelationDAO groupRelationDAO, GroupDao groupDao) {
        this.accountDAO = accountDAO;
        this.messageDAO = messageDAO;
        this.groupRelationDAO = groupRelationDAO;
        this.relationshipDAO = relationshipDAO;
        this.groupDao = groupDao;
    }

    @Transactional
    public Account createAccount(Account account) {
        logger.debug("Create new account with e-mail={}.", account.getEmail());
        if (account.getId() == 0) {
            return accountDAO.save(account);
        } else {
            throw new ValidationServiceException("Account with id=" + account.getId() + " already exists.");
        }
    }

    @Transactional
    public Account update(Account account) {
        logger.debug("Update account with e-mail={}, id={}.", account.getEmail(), account.getId());
        if (accountDAO.existsById(account.getId())) {
            if (account.getPhones() != null) {
                account.getPhones().removeIf(phone -> phone.getNumber() == null);
                account.getPhones().forEach(phone -> phone.setAccount(account));
            }
            return accountDAO.save(account);
        } else {
            throw new ValidationServiceException("Account with id=" + account.getId() + " not found");
        }
    }

    @Transactional
    @Secured("ROLE_ADMIN")
    public void delete(Account account) {
        logger.debug("Delete account with id={}.", account.getId());
        relationshipDAO.deleteRelations(account.getId());
        messageDAO.deleteAllByAccountFromOrAccountTo(account, account);
        accountDAO.deleteById(account.getId());
    }

    public Account get(int id) {
        logger.debug("Get account with id={}.", id);
        Optional<Account> optional = accountDAO.findById(id);
        if (!optional.isPresent()) {
            throw new ValidationServiceException("Account with id=" + id + " not found.");
        }
        return optional.get();
    }

    public Account get(String email) {
        logger.debug("Get account with e-mail={}.", email);
        return accountDAO.findByEmail(email);
    }

    public UserDetails findByUsername(String username) {
        logger.debug("Get UserDetails with username={}.", username);
        Account account = get(username);
        return new AccountDTO(account);
    }

    public List<Account> getByString(String string) {
        logger.debug("Get accounts for string={}.", string);
        return accountDAO.findAllByString(string, Sort.by("name"));
    }

    @Transactional
    public Account uploadPhoto(byte[] logo, Account account) {
        account.setPhoto(logo);
        return update(account);
    }

    public byte[] getPhoto(int id) {
        logger.debug("Get photo to account with id={}.", id);
        Account account = get(id);
        return account != null ? account.getPhoto() : null;
    }

    public List<Group> getUserGroups(int accountId) {
        logger.debug("Get groups where account with id={} is user.", accountId);
        List<GroupRelation> relations = groupRelationDAO.getByAccountId(accountId);
        List<Group> groups = new ArrayList<>();
        relations.forEach(relation -> {
            if (relation.getAccount().getId() == accountId && relation.getStatus() == GroupStatus.USER)
                groups.add(groupDao.get(relation.getGroup().getId()));
        });
        return groups;
    }

    public List<Group> getAdminGroups(int accountId) {
        logger.debug("Get groups where account with id={} is admin.", accountId);
        List<GroupRelation> relations = groupRelationDAO.getByAccountId(accountId);
        List<Group> groups = new ArrayList<>();
        relations.forEach(relation -> {
            if (relation.getAccount().getId() == accountId && relation.getStatus() == GroupStatus.ADMIN)
                groups.add(groupDao.get(relation.getGroup().getId()));
        });
        return groups;
    }

    public List<Group> getGroups(int accountId) {
        logger.debug("Get groups for account with id={}.", accountId);
        List<GroupRelation> relations = groupRelationDAO.getByAccountId(accountId);
        List<Group> groups = new ArrayList<>();
        relations.forEach(relation -> groups.add(groupDao.get(relation.getGroup().getId())));
        return groups;
    }

    public FriendRelation getFriendRelation(int accountId, int friendId) {
        logger.debug("Get friend relation for account with id={} and {}.", accountId, friendId);
        return relationshipDAO.getByTwoId(accountId, friendId);
    }

    public List<Account> getFriends(int accountId) {
        logger.debug("Get friends for account with id={}.", accountId);
        List<FriendRelation> friendRelations = relationshipDAO.getFriends(accountId);
        return getRelatedAccounts(friendRelations, accountId);
    }

    public List<Account> getSentRequests(int accountId) {
        logger.debug("Get users with requests from account with id={}.", accountId);
        List<FriendRelation> friendRelations = relationshipDAO.getSentRequests(accountId);
        return getRelatedAccounts(friendRelations, accountId);
    }

    public List<Account> getReceivedRequests(int accountId) {
        logger.debug("Get users with requests to account with id={}.", accountId);
        List<FriendRelation> friendRelations = relationshipDAO.getReceivedRequests(accountId);
        return getRelatedAccounts(friendRelations, accountId);
    }

    public List<Account> getDeclinedRequests(int accountId) {
        logger.debug("Get declined users by account with id={}.", accountId);
        List<FriendRelation> friendRelations = relationshipDAO.getDeclinedRequests(accountId);
        return getRelatedAccounts(friendRelations, accountId);
    }

    public List<Account> getBlocked(int accountId) {
        logger.debug("Get blocked users by account with id={}.", accountId);
        List<FriendRelation> friendRelations = relationshipDAO.getBlocked(accountId);
        return getRelatedAccounts(friendRelations, accountId);
    }

    @Transactional
    private FriendRelation insertFriendRelation(int accountId, int friendId, FriendStatus status) {
        try {
            FriendRelation relation = new FriendRelation();
            if (accountId > friendId) {
                relation.setAccountOne(get(friendId));
                relation.setAccountTwo(get(accountId));
            } else {
                relation.setAccountOne(get(accountId));
                relation.setAccountTwo(get(friendId));
            }
            relation.setActionAccount(get(accountId));
            relation.setStatus(status);
            return relationshipDAO.insert(relation);
        } catch (ValidationDAOException e) {
            throw new ValidationServiceException(e);
        }
    }

    @Transactional
    public FriendRelation sendFriendRequest(int accountId, int friendId) {
        logger.debug("Send friend request from account with id={} to account with id={}.", accountId, friendId);
        return insertFriendRelation(accountId, friendId, FriendStatus.PENDING);
    }

    @Transactional
    public void acceptFriendRequest(int accountId, int friendI) {
        logger.debug("Accept friend request from account with id={} to account with id={}.", friendI, accountId);
        FriendRelation relation = relationshipDAO.getByTwoId(accountId, friendI);
        if (relation != null) {
            int actionId = relation.getActionAccount().getId();
            if ((actionId != accountId && relation.getStatus() == FriendStatus.PENDING ||
                    actionId == accountId && relation.getStatus() == FriendStatus.DECLINED)) {
                relation.setStatus(FriendStatus.ACCEPTED);
                relation.setActionAccount(get(accountId));
            } else {
                throw new ValidationServiceException("Account with id=" + friendI +
                        " did not send request to user with id=" + accountId + ".");
            }
        } else {
            throw new ValidationServiceException("Relation between accounts with id=" + accountId + " and " +
                    friendI + " not found.");
        }
    }

    @Transactional
    public void declineFriendRequest(int accountId, int friendI) {
        logger.debug("Accept friend request from account with id={} to account with id={}.", friendI, accountId);
        FriendRelation relation = relationshipDAO.getByTwoId(accountId, friendI);
        if (relation != null) {
            int actionId = relation.getActionAccount().getId();
            if (actionId != accountId && relation.getStatus() == FriendStatus.PENDING) {
                relation.setStatus(FriendStatus.DECLINED);
                relation.setActionAccount(get(accountId));
            } else {
                throw new ValidationServiceException("Account with id=" + friendI +
                        " did not send request to user with id=" + accountId + ".");
            }
        } else {
            throw new ValidationServiceException("Relation between accounts with id=" + accountId + " and " +
                    friendI + " not found.");
        }
    }

    @Transactional
    public void blockUser(int accountId, int blockAccountId) {
        logger.debug("Account with id={} block account with id={}.", accountId, blockAccountId);
        FriendRelation relation = relationshipDAO.getByTwoId(accountId, blockAccountId);
        if (relation == null) {
            insertFriendRelation(accountId, blockAccountId, FriendStatus.BLOCKED);
        } else {
            if (relation.getStatus() != FriendStatus.BLOCKED) {
                relation.setStatus(FriendStatus.BLOCKED);
                relation.setActionAccount(get(accountId));
            } else {
                throw new ValidationServiceException("Account with id=" + blockAccountId +
                        " already blocked by user with id=" + accountId + ".");
            }
        }
    }

    @Transactional
    public int deleteFriendRelation(int accountId, int friendI) {
        logger.debug("Delete friend relation between accounts with id={} and id={}.", accountId, friendI);
        FriendRelation relation = relationshipDAO.getByTwoId(accountId, friendI);
        if (relation != null && !(relation.getStatus() == FriendStatus.BLOCKED &&
                relation.getActionAccount().getId() != accountId)) {
            try {
                return relationshipDAO.delete(relation.getId());
            } catch (ValidationDAOException e) {
                throw new ValidationServiceException(e);
            }
        } else {
            throw new ValidationServiceException("Relation between accounts with id=" + accountId + " and " +
                    friendI + " not found.");
        }
    }

    private List<Account> getRelatedAccounts(List<FriendRelation> friendRelations, int accountId) {
        List<Account> accounts = new ArrayList<>();
        friendRelations.forEach(relation -> accounts.add(accountDAO.findById
                (relation.getAccountOne().getId() == accountId ?
                        relation.getAccountTwo().getId() : relation.getAccountOne().getId()).get()));
        return accounts;
    }

    public List<Account> paginationSearch(String string, int page, int pageSize) {
        logger.debug("Pagination search account with string={}, in page={}, number of entries per page={}.",
                string, page, pageSize);
        return accountDAO.findAllByString(string, PageRequest.of(page, pageSize, Sort.by("name")));
    }

    public int getCountFromSearch(String string) {
        return getByString(string).size();
    }

    @Transactional
    public Message createMessage(Message message) {
        logger.debug("Create new {} message from account with id={} to account with id={}.", message.getType(),
                message.getAccountFrom().getId(), message.getAccountTo().getId());
        if (message.getId() == 0) {
            return messageDAO.save(message);
        } else {
            throw new ValidationServiceException("Message with id=" + message.getId() + " already exists.");
        }
    }

    public Message getMessage(int id) {
        logger.debug("Get message with id={}.", id);
        Optional<Message> optional = messageDAO.findById(id);
        if (!optional.isPresent()) {
            throw new ValidationServiceException("Account with id=" + id + " not found.");
        }
        return optional.get();
    }

    public List<Message> getMessages(MessageType type, int account1, int account2) {
        logger.debug("Get {} messages between accounts with id={} and id={}.", type, account1, account2);
        return messageDAO.findByTypeAndAccounts(MessageType.PERSONAL, account1, account2, Sort.by(Sort.Direction.DESC, "creationDate"));
    }

    public List<Message> getMessagesToAccount(MessageType type, Account accountTo) {
        logger.debug("Get {} messages to account with id={}.", type, accountTo.getId());
        return messageDAO.findByTypeAndAccountTo(type, accountTo, Sort.by(Sort.Direction.DESC, "creationDate"));
    }
}