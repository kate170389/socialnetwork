package com.getjavajob.makhova.security;

import com.getjavajob.makhova.common.Account;
import com.getjavajob.makhova.common.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class AccountDTO implements UserDetails {
    private String password;
    private String username;
    private Role role;

    private Account account;

    public AccountDTO(Account account) {
        if (account == null) {
            this.username = null;
            this.password = null;
            this.role = null;
        } else {
            this.username = account.getEmail();
            this.password = account.getPassword();
            this.role = account.getRole();
        }
        this.account = account;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_" + this.role));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Account getAccount() {
        return account;
    }
}