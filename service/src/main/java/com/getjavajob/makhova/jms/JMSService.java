package com.getjavajob.makhova.jms;

import com.getjavajob.makhova.common.JMSFriendRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class JMSService {
    private JmsTemplate jmsTemplate;

    @Autowired
    public JMSService(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendMessage(JMSFriendRequest jmsAccount) {
        jmsTemplate.convertAndSend("test", jmsAccount);
    }
}